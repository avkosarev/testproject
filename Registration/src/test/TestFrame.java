package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.awt.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.krysalis.barcode4j.tools.UnitConv;

import javafx.scene.control.SelectionMode;
import test.SelectDayHallSection;
import test.HttpsClient;
import test.MyBean;
import test.MyTableModel;




public class TestFrame extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static MouseMove MM;
	
	public static String FirstName;
	public static String SecondName;
	public static String ThirdName;
	public static String City;
	public static String RequestPath;
	
	public static JFrame frame = new JFrame("����������� ����������");
	public static JPanel panel = new JPanel();
	public static JTextField text_firstname=new JTextField();
	public static JTextField text_secondname=new JTextField();
	public static JTextField text_thirdname=new JTextField();
	public static JTextField text_cityorcountry=new JTextField();
	public static JLabel firstname=new JLabel("���");
	public static JLabel secondname=new JLabel("�������");
	public static JLabel thirdname=new JLabel("��������");
	public static JLabel cityorcountry=new JLabel("�����");
	public static JButton get_but = new JButton("������ ������");
	public static JButton but_search_user = new JButton("�����");
	public static JButton but_clear = new JButton("��������");
	public static JButton link = new JButton("������ ����������");
	public static JRadioButton edit = new JRadioButton("��������");
	public static JRadioButton printbadge = new JRadioButton("������ ��");
	public static JRadioButton sotrudnik = new JRadioButton("���������");
	static TableModel model;
    static JTable table;
    static JScrollPane scrollPane;
    static ArrayList<MyBean> beans = new ArrayList<MyBean>();
	
    static String BarCode;
    static String CompID;
    static String USER_ID = "";
    static String IsSotrudnik = "";
    
    static ListSelectionModel selectionModel;
    
    public static String[][] arHideDataUsers;
    
 
      
public static  void createGUI() {
	
	BufferedReader readerComp = null;
    try {
    	readerComp = new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream("CompID.txt"), Charset.forName("UTF-8")));
        String line;
        while ((line = readerComp.readLine()) != null) {
        	CompID = line;
        }
    } catch (IOException e) {
        // log error
    } finally {
        if (readerComp != null) {
            try {
            	readerComp.close();
            } catch (IOException e) {
                // log warning
            }
        }
    }
    BufferedReader readerPath = null;
    try {
    	readerPath = new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream("Path.txt"), Charset.forName("UTF-8")));
        String line;
        while ((line = readerPath.readLine()) != null) {
        	RequestPath = line;
        }
    } catch (IOException e) {
        // log error
    } finally {
        if (readerPath != null) {
            try {
            	readerPath.close();
            } catch (IOException e) {
                // log warning
            }
        }
    }
	MM = new MouseMove();
	MM.frame.setVisible(false);
    BufferedReader reader = null;
    int FileRow = 0;
    try {
        reader = new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream("Parameters.txt"), Charset.forName("UTF-8")));
        String line;
        while ((line = reader.readLine()) != null) {
       	 String[] Row = line.split("=");
            MouseMove.Parameters[FileRow++] = Row[1];
        }
    } catch (IOException e) {
        // log error
    } finally {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                // log warning
            }
        }
    }
    MouseMove.bW.setText(MouseMove.Parameters[0]);
    MouseMove.bH.setText(MouseMove.Parameters[1]);
    MouseMove.Font1.setText(MouseMove.Parameters[2]);
    MouseMove.Font2.setText(MouseMove.Parameters[3]);
    MouseMove.Font3.setText(MouseMove.Parameters[4]);
    MouseMove.F_top.setText(MouseMove.Parameters[5]);
    MouseMove.I_top.setText(MouseMove.Parameters[6]);
    MouseMove.O_top.setText(MouseMove.Parameters[7]);
    MouseMove.CITY_top.setText(MouseMove.Parameters[8]);
    MouseMove.brcdY.setText(MouseMove.Parameters[9]);
    MouseMove.brcdW.setText(MouseMove.Parameters[10]);
    MouseMove.brcdH.setText(MouseMove.Parameters[11]);
    badgePanel.bW = Integer.parseInt(MouseMove.bW.getText());
    badgePanel.bH = Integer.parseInt(MouseMove.bH.getText());
    if(badgePanel.bW > badgePanel.bH)
	{
    	MouseMove.bP.setPreferredSize(new Dimension(UnitConv.mm2px(badgePanel.bH,MouseMove.Res),UnitConv.mm2px(badgePanel.bW,MouseMove.Res)));
	}
	else
	{
		MouseMove.bP.setPreferredSize(new Dimension(UnitConv.mm2px(badgePanel.bW,MouseMove.Res),UnitConv.mm2px(badgePanel.bH,MouseMove.Res)));
	}
    badgePanel.Font_F = Integer.parseInt(MouseMove.Font1.getText());
    badgePanel.Font_IO = Integer.parseInt(MouseMove.Font2.getText());
    badgePanel.Font_CITY = Integer.parseInt(MouseMove.Font3.getText());
    badgePanel.F_top = UnitConv.mm2px(Integer.parseInt(MouseMove.F_top.getText()),MouseMove.Res);
    badgePanel.I_top = UnitConv.mm2px(Integer.parseInt(MouseMove.I_top.getText()),MouseMove.Res);
    badgePanel.O_top = UnitConv.mm2px(Integer.parseInt(MouseMove.O_top.getText()),MouseMove.Res);
    badgePanel.CITY_top = UnitConv.mm2px(Integer.parseInt(MouseMove.CITY_top.getText()),MouseMove.Res);
    badgePanel.brcd_y = UnitConv.mm2px(Integer.parseInt(MouseMove.brcdY.getText()),MouseMove.Res);
    badgePanel.brcd_width = UnitConv.mm2px(Integer.parseInt(MouseMove.brcdW.getText()),MouseMove.Res);
    badgePanel.brcd_height = UnitConv.mm2px(Integer.parseInt(MouseMove.brcdH.getText()),MouseMove.Res);
    
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int sizeWidth = screenSize.width;
	int sizeHeight = screenSize.height;
	panel.setPreferredSize(screenSize);
	panel.setLayout(null);
	frame.add(new JScrollPane(panel));
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    firstname.setFont(new Font("Arial", Font.PLAIN, 30));
    secondname.setFont(new Font("Arial", Font.PLAIN, 30));
    thirdname.setFont(new Font("Arial", Font.PLAIN, 30));
    cityorcountry.setFont(new Font("Arial", Font.PLAIN, 30));
    but_search_user.setFont(new Font("Arial", Font.PLAIN, 20));
    but_clear.setFont(new Font("Arial", Font.PLAIN, 20));
    edit.setFont(new Font("Arial", Font.PLAIN, 20));
    edit.setEnabled(false);
    printbadge.setFont(new Font("Arial", Font.PLAIN, 20));
    printbadge.setSelected(true);
    sotrudnik.setFont(new Font("Arial", Font.PLAIN, 20));
    sotrudnik.setSelected(false);
    text_firstname.setFont(new Font("Arial", Font.PLAIN, 30));
    text_secondname.setFont(new Font("Arial", Font.PLAIN, 30));
    text_thirdname.setFont(new Font("Arial", Font.PLAIN, 30));
    text_cityorcountry.setFont(new Font("Arial", Font.PLAIN, 30));
    
    get_but.setFont(new Font("Arial", Font.PLAIN, 30));
    link.setFont(new Font("Arial", Font.PLAIN, 20));
          
          int interval = 60;
          secondname.setBounds(20,200,sizeWidth/4-20,50);
          panel.add(secondname);
          text_secondname.setBounds(sizeWidth/4,200,sizeWidth/2-20,50);
          panel.add(text_secondname);
          firstname.setBounds(20,200+interval,sizeWidth/4-20,50);
          panel.add(firstname);
          text_firstname.setBounds(sizeWidth/4,200+interval,sizeWidth/2-20,50);
          panel.add(text_firstname);
          interval+=60;
          thirdname.setBounds(20,200+interval,sizeWidth/4-20,50);
          panel.add(thirdname);
          text_thirdname.setBounds(sizeWidth/4,200+interval,sizeWidth/2-20,50);
          panel.add(text_thirdname);
          interval+=60;
          cityorcountry.setBounds(20,200+interval,sizeWidth/4-20,50);
          panel.add(cityorcountry);
          text_cityorcountry.setBounds(sizeWidth/4,200+interval,sizeWidth/2-20,50);
          panel.add(text_cityorcountry);
          interval+=60;
          but_search_user.setBounds(40,200+interval,150,50);
          panel.add(but_search_user);
          but_clear.setBounds(200,200+interval,150,50);
          panel.add(but_clear);
          edit.setBounds(360,200+interval,150,50);
          panel.add(edit);
          printbadge.setBounds(360+160,200+interval,150,50);
          panel.add(printbadge);
          sotrudnik.setBounds(360+160+160,200+interval,150,50);
          panel.add(sotrudnik);
      	  interval+=60;
      	  model = new MyTableModel(beans);
          table = new JTable(model);
          selectionModel = table.getSelectionModel();
          table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          scrollPane = new JScrollPane(table);
          table.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 30));
          table.setFont(new Font("Arial", Font.PLAIN, 20));
          table.getColumnModel().getColumn(0).setWidth(sizeWidth-20);
          table.setRowHeight(40);
          table.setFillsViewportHeight(true);
          scrollPane.setBounds(10,200+interval,sizeWidth-40,sizeHeight - 200 - interval);
          panel.add(scrollPane);
          
          get_but.setBounds(3*sizeWidth/4,200+interval-100-70,sizeWidth/4-50,100);
          panel.add(get_but);
          link.setBounds(3*sizeWidth/4,200+interval-100-70-100-20,sizeWidth/4-50,100);
          panel.add(link);
          JLabel imageLabel = new JLabel(new ImageIcon("logo.png"));
          imageLabel.setHorizontalAlignment(SwingConstants.LEFT);
          imageLabel.setBounds(10, 10, sizeWidth, 150);
          panel.add(imageLabel);
          JLabel Title = new JLabel("����������� ����������");
          Title.setFont(new Font("Arial", Font.PLAIN, 50));
          Title.setHorizontalAlignment(SwingConstants.CENTER);
          Title.setBounds(10, 10, sizeWidth, 100);
          panel.add(Title);
          frame.setSize(sizeWidth,sizeHeight);
          frame.setVisible(true);
          
          selectionModel.addListSelectionListener(new ListSelectionListener() {
	  		    public void valueChanged(ListSelectionEvent e)
	  		    {
	  		    	int rowIndex = table.getSelectedRow();
	  		    	if(rowIndex >=0)
	  		    	{
	  		    		 text_firstname.setText(arHideDataUsers[rowIndex][2].replace(" ", ""));	
	  		    		 text_secondname.setText(arHideDataUsers[rowIndex][1].replace(" ", ""));
	  		    		 text_thirdname.setText(arHideDataUsers[rowIndex][3].replace(" ", ""));
	  		    		 text_cityorcountry.setText(arHideDataUsers[rowIndex][4].replace(" ", ""));
	  		    		 USER_ID = arHideDataUsers[rowIndex][0];
	  		    		 edit.setEnabled(true);
	  		    	}
	  		    	else
	  		    	{
	  		    		text_firstname.setText("");	
	  		    		text_secondname.setText("");
	  		    		text_thirdname.setText("");
	  		    		text_cityorcountry.setText("");
	  		    		USER_ID = "";
	  		    		edit.setSelected(false);
	  		    		edit.setEnabled(false);
	  		    	}
	  		    }
	  		});
          
          text_firstname.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					  USER_ID = "";
		              if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_W)
		                 	new SelectDayHallSection();
						if(e.getKeyCode() == KeyEvent.VK_ENTER)
						{
							but_search_user.doClick();
						}
						if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
						{
							but_clear.doClick();
						}
		          }
		      });
          text_secondname.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					  USER_ID = "";
		              if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_W)
		                 	new SelectDayHallSection();
						if(e.getKeyCode() == KeyEvent.VK_ENTER)
						{
							but_search_user.doClick();
						}
						if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
						{
							but_clear.doClick();
						}
		          }
		      });
          text_thirdname.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					  USER_ID = "";
		              if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_W)
		                 	new SelectDayHallSection();
						if(e.getKeyCode() == KeyEvent.VK_ENTER)
						{
							but_search_user.doClick();
						}
						if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
						{
							but_clear.doClick();
						}
		          }
		      });
          text_cityorcountry.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
		              if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_W)
		                 	new SelectDayHallSection();
						if(e.getKeyCode() == KeyEvent.VK_ENTER)
						{
							but_search_user.doClick();
						}
						if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
						{
							but_clear.doClick();
						}
		          }
		      });
          get_but.addActionListener(new ActionListener() {
          	
              public void actionPerformed(ActionEvent e) {  
             	 try{
             		 FirstName=text_firstname.getText();
                	 SecondName=text_secondname.getText();
                	 ThirdName=text_thirdname.getText();
                	 City=text_cityorcountry.getText();
                	 if(FirstName.length()==0 || SecondName.length()==0 || /*ThirdName.length()==0 ||*/ City.length()==0) 
                		 JOptionPane.showMessageDialog(null, "��������� ����!"); 
                	 else
                	 {
                		 Main m1 = new Main();
                		 String SendRequest = "";
                		 if(edit.isSelected() && table.getSelectedRow()>=0)
                		 {
                			 USER_ID = arHideDataUsers[table.getSelectedRow()][0];
                		 }
                		 if(sotrudnik.isSelected())
                		 {
                			 IsSotrudnik = "Y";
                		 }
                		 SendRequest = HttpsClient.testIt("?SOTRUDNIK="+IsSotrudnik+"&USER_ID="+USER_ID+"&COMP_ID="+CompID+"&NAME="+URLEncoder.encode(FirstName,"UTF-8")+"&FIRST_NAME="+URLEncoder.encode(SecondName,"UTF-8")+"&SECOND_NAME="+URLEncoder.encode(ThirdName,"UTF-8")+"&CITY="+URLEncoder.encode(City,"UTF-8"));
                		 if(SendRequest.length() == 13)
                		 {
                			 BarCode = SendRequest.substring(0,12);
                			 m1.startJob();
		 	              	 text_firstname.setText("");
		 	              	 text_secondname.setText("");
		 	              	 text_thirdname.setText("");
		 	              	 text_cityorcountry.setText("");
		 	              	 edit.setSelected(false);
		 	              	 USER_ID="";
		 	              	 IsSotrudnik="";
		 	              	 beans.removeAll(beans);
		 	              	 table.clearSelection();
		 	              	 edit.setEnabled(false);
                		 }
                		 else
                		 {
                			 if(edit.isSelected() && table.getSelectedRow()>=0)
                    		 {
                				 BarCode = arHideDataUsers[table.getSelectedRow()][5];
                    			 String oldName = beans.get(table.getSelectedRow()).getName();
                    			 String oldCity = beans.get(table.getSelectedRow()).getCity();
                    			 String newName = SecondName + " " + FirstName + " " + ThirdName;
                    			 String newCity = City;
                    			 List<String> UsersFromLogs = new ArrayList<String>();
                    			 BufferedReader UsersFromLogsReader = null;
                    		
                    			    	try {
                    			    		UsersFromLogsReader = new BufferedReader(
    										            new InputStreamReader(
    										                new FileInputStream("LOGS.txt")));
    									} catch (FileNotFoundException e1) {
    										// TODO Auto-generated catch block
    										e1.printStackTrace();
    									}
                    			        String log;
                    			        try {
    										while ((log = UsersFromLogsReader.readLine()) != null) {
    											if(log.indexOf(oldName)!=-1 && log.indexOf(oldCity)!=-1)
    											{
    												log = log.replace(oldName, newName);
    												log = log.replace(oldCity, newCity);
    											}
    											UsersFromLogs.add(log);
    										}
    									} catch (IOException e1) {
    										// TODO Auto-generated catch block
    										e1.printStackTrace();
    									}
                    			        String newUsersFromLogs = String.join(System.getProperty("line.separator"), UsersFromLogs);
                    			        MouseMove.write("LOGS.txt",newUsersFromLogs+System.getProperty("line.separator")); 
                    			        m1.startJob();
	       			 	              	 text_firstname.setText("");
	       			 	              	 text_secondname.setText("");
	       			 	              	 text_thirdname.setText("");
	       			 	              	 text_cityorcountry.setText("");
	       			 	              	 edit.setSelected(false);
	       			 	              	 USER_ID="";
	       			 	              	 IsSotrudnik="";
	       			 	              	 beans.removeAll(beans);
	       			 	              	 table.clearSelection();
	       			 	              	 edit.setEnabled(false);
                    		 }
                			 else
                			 {
                				 if(table.getSelectedRow()>=0 && USER_ID.length() > 0)
                				 {
                					 BarCode = arHideDataUsers[table.getSelectedRow()][5];
                					 m1.startJob();
    			 	              	 text_firstname.setText("");
    			 	              	 text_secondname.setText("");
    			 	              	 text_thirdname.setText("");
    			 	              	 text_cityorcountry.setText("");
    			 	              	 edit.setSelected(false);
    			 	              	 USER_ID="";
    			 	              	 IsSotrudnik="";
    			 	              	 beans.removeAll(beans);
    			 	              	 table.clearSelection();
    			 	              	 edit.setEnabled(false);
                				 }
                				 else
                				 {
                    			 List<String> PoolBarcodes = new ArrayList<String>();
                    			 BufferedReader Pool = null;
                    		
                    			    	try {
    										Pool = new BufferedReader(
    										            new InputStreamReader(
    										                new FileInputStream("Pool.txt"), Charset.forName("UTF-8")));
    									} catch (FileNotFoundException e1) {
    										// TODO Auto-generated catch block
    										e1.printStackTrace();
    									}
                    			        String line;
                    			        try {
    										while ((line = Pool.readLine()) != null) {
    											if(line.length() != 12) continue;
    											PoolBarcodes.add(line);
    										}
    									} catch (IOException e1) {
    										// TODO Auto-generated catch block
    										e1.printStackTrace();
    									}
                    			 if((PoolBarcodes.size() > 0 && TestFrame.printbadge.isSelected()) || !TestFrame.printbadge.isSelected())
                    			 {
                    				 if(TestFrame.printbadge.isSelected())
                    				 {
                    					 Random r = new Random();
                            			 int El = (PoolBarcodes.size()==1)?0:r.nextInt(PoolBarcodes.size()-1);
                            			 BarCode = PoolBarcodes.get(El);
                            			 PoolBarcodes.remove(El);
                            			 Locale local = new Locale("ru","RU");
                            			 DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, local); 
                            			 Date currentDate = new Date(); 
                            			 String date = df.format(currentDate);
                        				 appendUsingFileWriter("LOGS.txt",SecondName+" "+FirstName+" "+ThirdName+"XXXXX"+City+"XXXXX"+BarCode+"XXXXX"+date+"XXXXX"+"���������:"+IsSotrudnik+System.getProperty("line.separator"));
                            			 String newPool = String.join(System.getProperty("line.separator"), PoolBarcodes);
                            			 MouseMove.write("Pool.txt",newPool);
                    				 }
                    				 else
                    					 BarCode = "111111111111";
		                			 m1.startJob();
				 	              	 text_firstname.setText("");
				 	              	 text_secondname.setText("");
				 	              	 text_thirdname.setText("");
				 	              	 text_cityorcountry.setText("");
				 	              	 edit.setSelected(false);
				 	              	 USER_ID="";
				 	              	 IsSotrudnik="";
				 	              	 beans.removeAll(beans);
				 	              	 table.clearSelection();
				 	              	 edit.setEnabled(false);
	                			 }
	                			 else
	                				 JOptionPane.showMessageDialog(null, "� ���� ����������� �����-����");
                				 }
	                	 		  }
                		 }
	 	              	 frame.repaint();
                	 }
                	 } catch(NumberFormatException nfe)  { } catch (UnsupportedEncodingException e1) {
					}
      
              }
         });
          
         but_search_user.addActionListener(new ActionListener() {
            	
              public void actionPerformed(ActionEvent e) {
            	table.clearSelection();  	
            	if(text_secondname.getText().length() > 0 || text_firstname.getText().length() > 0 || text_thirdname.getText().length() >0)
            	{
            	  beans.removeAll(beans);
            	  String response = "";
					try {
						response = HttpsClient.testIt("?SEARCH=Y&COMP_ID="+CompID+"&SEARCH_FIRST_NAME="+URLEncoder.encode(text_secondname.getText(),"UTF-8")+"&SEARCH_NAME="+URLEncoder.encode(text_firstname.getText(),"UTF-8")+"&SEARCH_SECOND_NAME="+URLEncoder.encode(text_thirdname.getText(),"UTF-8"));
						String[] arUsers = (response.length()>0)?response.split("X{5}"):(new String[0]);
						arHideDataUsers = new String[arUsers.length][];
						for(int iUser=0;iUser<arUsers.length;iUser++)
						{
							String[] usersString = arUsers[iUser].split("Z{5}");
							beans.add(new MyBean(usersString[1]+" "+usersString[2]+" "+usersString[3],usersString[4],usersString[5],usersString[6],usersString[7]));
							arHideDataUsers[iUser] = new String[usersString.length];
							arHideDataUsers[iUser][0] = usersString[0];
							arHideDataUsers[iUser][1] = usersString[1];
							arHideDataUsers[iUser][2] = usersString[2];
							arHideDataUsers[iUser][3] = usersString[3];
							arHideDataUsers[iUser][4] = usersString[4];
							arHideDataUsers[iUser][5] = usersString[5];
							arHideDataUsers[iUser][6] = usersString[6];
							arHideDataUsers[iUser][7] = usersString[7];
						}
						Process proc = Runtime.getRuntime().exec("ping -n 1 "+RequestPath);
				        boolean reachable = (proc.waitFor()==0);
						if(response.length() == 0 && !reachable)
						{
							List<String> Users = new ArrayList<String>();
	           			 	BufferedReader usersReader = null;
	           		
	           			    	try {
	           			    		usersReader = new BufferedReader(
										            new InputStreamReader(
										                new FileInputStream("LOGS.txt")));
									} catch (FileNotFoundException e11) {
										// TODO Auto-generated catch block
										e11.printStackTrace();
									}
	           			        String line;
	           			        try {
										while ((line = usersReader.readLine()) != null) {
											if(line.length()==0)continue;
											if(line.indexOf(text_secondname.getText()) != -1)
											Users.add(line);
										}
									} catch (IOException e11) {
										// TODO Auto-generated catch block
										e11.printStackTrace();
									}
	           			        if(Users.size() >0)
	           			        {
	           			        	arHideDataUsers = new String[Users.size()][];
	        						for(int iUser=0;iUser<Users.size();iUser++)
	        						{
	        							String[] usersString = Users.get(iUser).split("X{5}");
	        							String[] FIO = new String[3];
	        							String arFIO[] = usersString[0].split(" ");
	        							for(int i=0; i<arFIO.length; i++)
	        								FIO[i] = arFIO[i];
	        							if(arFIO.length == 2)
	        								FIO[2] = "";
	        							beans.add(new MyBean(usersString[0],usersString[1]," "," "," "));
	        							arHideDataUsers[iUser] = new String[8];
	        							arHideDataUsers[iUser][0] = Integer.toString(iUser);
	        							arHideDataUsers[iUser][1] = FIO[0];
	        							arHideDataUsers[iUser][2] = FIO[1];
	        							arHideDataUsers[iUser][3] = FIO[2];
	        							arHideDataUsers[iUser][4] = usersString[1];
	        							arHideDataUsers[iUser][5] = usersString[2];
	        							arHideDataUsers[iUser][6] = " ";
	        							arHideDataUsers[iUser][7] = " ";
	        						}
	           			        }
	           			        
						}
					} catch (UnsupportedEncodingException e1) {	
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				  scrollPane.updateUI();
            	  frame.repaint();
              }
              }
          });
         but_clear.addActionListener(new ActionListener() {
         	
             public void actionPerformed(ActionEvent e) {
	              	 text_firstname.setText("");
	              	 text_secondname.setText("");
	              	 text_thirdname.setText("");
	              	 text_cityorcountry.setText("");
	              	 USER_ID="";
	              	 IsSotrudnik="";
	              	 table.clearSelection();
             }
             });
         link.addActionListener(new ActionListener(){
        	 
        	 public void actionPerformed(ActionEvent actionEvent){
        		  try {                
        		         Desktop.getDesktop().browse(URI.create(RequestPath+"?LIST=Y&COMP_ID="+CompID));
        		      }catch(IOException e) {
        		       e.printStackTrace();
        		      }
        		}
			});
         
}
// ��������� ���������� � ���� � ������� FileWriter
private static void appendUsingFileWriter(String filePath, String text) {
    File file = new File(filePath);
    FileWriter fr = null;
    try {
        fr = new FileWriter(file,true);
        fr.write(text);
       
    } catch (IOException e) {
        e.printStackTrace();
    }finally{
        try {
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
public static void main(String[] args) throws ClassNotFoundException, SQLException { 
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
         public void run() {
              createGUI();
             
         }
    });
}

}
