package test;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.io.*;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

public class HttpsClient{


   public static String testIt(String Data){
	   String response="";
      String https_url = TestFrame.RequestPath+ Data;
      URL url;
      try {

	     url = new URL(https_url);
	     HttpsURLConnection con = (HttpsURLConnection)url.openConnection();


	     //dump all the content
	     response = print_content(con);

      } catch (MalformedURLException e) {
	     e.printStackTrace();
      } catch (IOException e) {
	     e.printStackTrace();
      }
      return response;

   }


   public static String print_content(HttpsURLConnection con){
	 String response="";	
	if(con!=null){
	try {

	   //System.out.println("****** Content of the URL ********");
	   BufferedReader br =
		new BufferedReader(
			new InputStreamReader(con.getInputStream(),"Utf8"));

	   String input;

	   while ((input = br.readLine()) != null){
	      //System.out.println(input);
	      response+=input;
	   }
	   br.close();

	} catch (IOException e) {
	   e.printStackTrace();
	}

       }
	return response;
   }

}
