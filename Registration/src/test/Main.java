package test;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.Size2DSyntax;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.OrientationRequested;

import org.krysalis.barcode4j.tools.UnitConv;

import com.barcodelib.barcode.Linear;
import com.businessrefinery.barcode.Barcode;
import com.onbarcode.barcode.EAN13;

import java.awt.Toolkit;





public class Main implements Printable {
	
	public static Paper paper;
	public static PageFormat PF;
	public static PrinterJob pj;
	public Main()
	{
		pj = PrinterJob.getPrinterJob();
    	PF = pj.defaultPage();
    	PF.setOrientation(PageFormat.PORTRAIT);
    	paper = PF.getPaper();
    	paper.setSize(UnitConv.mm2pt(210),UnitConv.mm2pt(297));
    	paper.setImageableArea(0,0,UnitConv.mm2pt(210),UnitConv.mm2pt(297));
    	if(badgePanel.bH >= badgePanel.bW)
    		PF.setOrientation(PageFormat.PORTRAIT);
    	else
    		PF.setOrientation(PageFormat.LANDSCAPE);
    	PF.setPaper(paper);
	}
    public int print(Graphics g, PageFormat pf, int pi)
    throws PrinterException {
        if (pi >= 1) {
            return Printable.NO_SUCH_PAGE;
        }
        try {
			drawText((Graphics2D) g);
        	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return Printable.PAGE_EXISTS;
    }
    public void drawCenteredString(Graphics g, String text, Rectangle rect, Font font, int y) {
        // Get the FontMetrics
        FontMetrics metrics = g.getFontMetrics(font);
        // Determine the X coordinate for the text
        int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
        // Set the font
        g.setFont(font);
        // Draw the String
        g.drawString(text, x, y);
    }
    public void drawText(Graphics2D g) throws IOException
    {
    	
        Font f1 = new Font("Tahoma", Font.BOLD, badgePanel.Font_F);
        Font f2 = new Font("Tahoma", Font.BOLD, badgePanel.Font_IO);
        Font f3 = new Font("Arial", Font.PLAIN, badgePanel.Font_CITY);
        g.setPaint(Color.black);
        g.setFont(f1);
        List<String> lines = new ArrayList<String>();
			   lines.add(TestFrame.SecondName);
			   lines.add(TestFrame.FirstName);
			   lines.add(TestFrame.ThirdName);
			   lines.add(TestFrame.City);
		String [] linesAsArray = lines.toArray(new String[lines.size()]);

		 //MyBarCodeUtility barCodeUtil = new MyBarCodeUtility();
		 
	   
	     // This will generate Bar-Code 128 format
	     //barCodeUtil.createBarCodeEAN13(String.valueOf(TestFrame.BarCode));
		/*Barcode barcode = new Barcode();
				barcode.setSymbology(Barcode.EAN13);
				barcode.setCode(TestFrame.BarCode);
				try {
					barcode.drawImage2File(String.valueOf(TestFrame.BarCode)+".JPG");
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} */
			   Linear linear = new Linear(); 
			       linear.setType(Linear.EAN13); 
			       linear.setData(String.valueOf(TestFrame.BarCode)); 
			       try {
					linear.renderBarcode(String.valueOf(TestFrame.BarCode)+".JPG");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
	     BufferedImage img = null;
	     try {
	         img = ImageIO.read(new File(String.valueOf(TestFrame.BarCode)+".JPG"));	         
	     } catch (IOException e) {
	     }
	     if(badgePanel.bH >= badgePanel.bW)
	     {
	        Rectangle r = new Rectangle((int) UnitConv.mm2pt(210),(int) UnitConv.mm2pt(297));
	        g.setFont(f1);
	        drawCenteredString(g,linesAsArray[0],r,f1,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.F_top.getText())));
	        g.setFont(f2);
	        drawCenteredString(g,linesAsArray[1],r,f2,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.I_top.getText())));
	        drawCenteredString(g,linesAsArray[2],r,f2,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.O_top.getText())));
	        g.setFont(f3);
	        drawCenteredString(g,linesAsArray[3],r,f3,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.CITY_top.getText())));
	        if(TestFrame.printbadge.isSelected())
	        	g.drawImage(img,(int) (r.width/2-UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdW.getText()))/2), (int) UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdY.getText())),(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdW.getText())), (int)UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdH.getText())),null);
	        (new File(String.valueOf(TestFrame.BarCode)+".JPG")).delete();
	     }
	     else
	     {
	    	 Rectangle r = new Rectangle((int) UnitConv.mm2pt(297-badgePanel.bW/2),(int) UnitConv.mm2pt((210-badgePanel.bH)/2),(int) UnitConv.mm2pt(badgePanel.bW/2),(int) UnitConv.mm2pt(badgePanel.bH));
		     g.setFont(f1);
		     drawCenteredString(g,linesAsArray[0],r,f1,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.F_top.getText()))+r.y);
		     g.setFont(f2);
		     drawCenteredString(g,linesAsArray[1],r,f2,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.I_top.getText()))+r.y);
		     drawCenteredString(g,linesAsArray[2],r,f2,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.O_top.getText()))+r.y);
		     g.setFont(f3);
		     drawCenteredString(g,linesAsArray[3],r,f3,(int) UnitConv.mm2pt(Integer.parseInt(MouseMove.CITY_top.getText()))+r.y);
		     if(TestFrame.printbadge.isSelected())
		    	 g.drawImage(img,(int)(r.x+UnitConv.mm2pt(badgePanel.bW/4)-UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdW.getText()))/2), (int) UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdY.getText()))+r.y, (int) UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdW.getText())), (int) UnitConv.mm2pt(Integer.parseInt(MouseMove.brcdH.getText())),null);
		     (new File(String.valueOf(TestFrame.BarCode)+".JPG")).delete();
	     }
    }
    void startJob() {
            pj.setPrintable(this,PF);
            pj.setCopies(pj.getCopies());
            pj.setJobName("Print your text...");
            try {
						pj.print();
			} catch (PrinterException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
			}
        }
}