package test;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SelectDayHallSection extends JFrame{
	JPanel select = new JPanel();
	JLabel lcompID = new JLabel("ID �����������");
	JTextField tcompID = new JTextField(TestFrame.CompID);
	JLabel lRequestPath = new JLabel("�������� ��� �������� �������:");
	JTextField tRequestPath = new JTextField(TestFrame.RequestPath);
	public static JButton badgesettings = new JButton("��������� ������");
	JButton OK = new JButton("���������");
	JButton CLOSE = new JButton("������� ����������");
	public SelectDayHallSection()
	{
		super("���������");
		lcompID.setPreferredSize(new Dimension(150,50));
		tcompID.setPreferredSize(new Dimension(400,50));	
		OK.setPreferredSize(new Dimension(200,50));
		CLOSE.setPreferredSize(new Dimension(200,50));
		lRequestPath.setPreferredSize(new Dimension(150,50));
		tRequestPath.setPreferredSize(new Dimension(400,50));
		badgesettings.setPreferredSize(new Dimension(400,50));
		select.add(lcompID);
		select.add(tcompID);
		select.add(lRequestPath);
		select.add(tRequestPath);
		select.add(badgesettings);
		select.add(OK);
		select.add(CLOSE);
        setContentPane(select);
        setSize(600,400);
        setVisible(true);
        OK.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	TestFrame.CompID = tcompID.getText();
		    	TestFrame.RequestPath = tRequestPath.getText();
		    	TestFrame.frame.repaint();
		    	dispose();
		    }
		    });
        badgesettings.addActionListener(new ActionListener() {
           	
            public void actionPerformed(ActionEvent e) {
           		TestFrame.MM.frame.setVisible(true);
            }});
        CLOSE.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {	
		    	System.exit(0);
		    }
		    });
        tcompID.addKeyListener(new KeyListener()
        		{

					@Override
					public void keyPressed(KeyEvent arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void keyReleased(KeyEvent evt) {
						// TODO Auto-generated method stub
						File file = new File("CompID.txt");
						 
					    try {
					        //���������, ��� ���� ���� �� ���������� �� ������� ���
					        if(!file.exists()){
					            file.createNewFile();
					        }
					 
					        //PrintWriter ��������� ����������� ������ � ����
					        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
					 
					        try {
					            //���������� ����� � ����
					            out.print(tcompID.getText());
					        } finally {
					            //����� ���� �� ������ ������� ����
					            //����� ���� �� ���������
					            out.close();
					        }
					    } catch(IOException e) {
					        throw new RuntimeException(e);
					    }
					}

					@Override
					public void keyTyped(KeyEvent arg0) {
						// TODO Auto-generated method stub
						
					}
        	
        		});
        tRequestPath.addKeyListener(new KeyListener()
		{

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent evt) {
				// TODO Auto-generated method stub
				File file = new File("Path.txt");
				 
			    try {
			        //���������, ��� ���� ���� �� ���������� �� ������� ���
			        if(!file.exists()){
			            file.createNewFile();
			        }
			 
			        //PrintWriter ��������� ����������� ������ � ����
			        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
			 
			        try {
			            //���������� ����� � ����
			            out.print(tRequestPath.getText());
			        } finally {
			            //����� ���� �� ������ ������� ����
			            //����� ���� �� ���������
			            out.close();
			        }
			    } catch(IOException e) {
			        throw new RuntimeException(e);
			    }
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
	
		});

	}
}
