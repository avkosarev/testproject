package test;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;

import org.krysalis.barcode4j.tools.UnitConv;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class MouseMove extends JFrame
{
  static String[] Parameters = new String[12];	
  static int Res = 100;
  static int minusX = 10;
  static int minusY = 50;
  static JFrame frame = new JFrame("��������� ������");
  
  static JPanel settingsPanel;
  
  JLabel Badge_label = new JLabel("������� ������ (�,�), ��");
  static JTextField bW = new JTextField();
  static JTextField bH = new JTextField();
  
  JLabel Font_label = new JLabel("������ (���, �����)");
  static JTextField Font1 = new JTextField();
  static JTextField Font2 = new JTextField();
  static JTextField Font3 = new JTextField();
  
  JLabel F_label = new JLabel("������� (������ ������), ��");
  static JTextField F_top = new JTextField();
  
  JLabel I_label = new JLabel("��� (������ ������), ��");
  static JTextField I_top = new JTextField();
  
  JLabel O_label = new JLabel("�������� (������ ������), ��");
  static JTextField O_top = new JTextField();
  
  JLabel CITY_label = new JLabel("����� (������ ������), ��");
  static JTextField CITY_top = new JTextField();
  
  JLabel BARCODE_label = new JLabel("�����-��� (������ ������, ������, ������), ��");
  static JTextField brcdY = new JTextField();
  static JTextField brcdW = new JTextField();
  static JTextField brcdH = new JTextField();
  static badgePanel bP;
  JScrollPane sp;
  public static void write(String fileName, String text) {
	    //���������� ����
	    File file = new File(fileName);
	 
	    try {
	        //���������, ��� ���� ���� �� ���������� �� ������� ���
	        if(!file.exists()){
	            file.createNewFile();
	        }
	 
	        //PrintWriter ��������� ����������� ������ � ����
	        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
	 
	        try {
	            //���������� ����� � ����
	            out.print(text);
	        } finally {
	            //����� ���� �� ������ ������� ����
	            //����� ���� �� ���������
	            out.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }
	}
  public MouseMove()
  {  
    frame.setSize(800,600);
    frame.setVisible(true);
    frame.setLayout(new BorderLayout());
    settingsPanel = new JPanel();
    bW.setText(bW.getText());
    bH.setText(bH.getText());
    Font1.setText(Font1.getText());
    Font2.setText(Font2.getText());
    Font3.setText(Font3.getText());
    F_top.setText(F_top.getText());
    I_top.setText(I_top.getText());
    O_top.setText(O_top.getText());
    CITY_top.setText(CITY_top.getText());
    brcdY.setText(brcdY.getText());
    brcdW.setText(brcdW.getText());
    brcdH.setText(brcdH.getText());
     
    bP = new badgePanel();
    settingsPanel.setBackground(Color.LIGHT_GRAY);
    GridLayout layout = new GridLayout(19, 1, 0, 0);
    settingsPanel.setLayout(layout);
    settingsPanel.add(Badge_label);
    settingsPanel.add(bW);
    settingsPanel.add(bH);
    settingsPanel.add(Font_label);
    settingsPanel.add(Font1);
    settingsPanel.add(Font2);
    settingsPanel.add(Font3);
    settingsPanel.add(F_label);
    settingsPanel.add(F_top);
    settingsPanel.add(I_label);
    settingsPanel.add(I_top);
    settingsPanel.add(O_label);
    settingsPanel.add(O_top);
    settingsPanel.add(CITY_label);
    settingsPanel.add(CITY_top);
    settingsPanel.add(BARCODE_label);
    settingsPanel.add(brcdY);
    settingsPanel.add(brcdW);
    settingsPanel.add(brcdH);
    
    sp = new JScrollPane(bP);
    
    frame.add(sp,BorderLayout.CENTER);
    frame.add(settingsPanel,BorderLayout.EAST);
    frame.addWindowListener(new WindowAdapter() {
    	public void windowClosing(WindowEvent e) {
    	frame.setVisible(false);
    	}
    	});
    bW.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.bW = Integer.parseInt(bW.getText());
		    if(badgePanel.bW > badgePanel.bH)
			{
		    	bP.setPreferredSize(new Dimension(UnitConv.mm2px(badgePanel.bH,Res),UnitConv.mm2px(badgePanel.bW,Res)));
				//bP.setBounds(minusX,minusY,UnitConv.mm2px(badgePanel.bH,Res),UnitConv.mm2px(badgePanel.bW,Res));
				//settingsPanel.setBounds(minusX+UnitConv.mm2px(badgePanel.bH+10,Res),minusY,UnitConv.mm2px(100,Res),UnitConv.mm2px(100,Res));
			}
			else
			{
				bP.setPreferredSize(new Dimension(UnitConv.mm2px(badgePanel.bW,Res),UnitConv.mm2px(badgePanel.bH,Res)));
				//bP.setBounds(minusX,minusY,UnitConv.mm2px(badgePanel.bW,Res),UnitConv.mm2px(badgePanel.bH,Res));
				//settingsPanel.setBounds(minusX+UnitConv.mm2px(badgePanel.bW+10,Res),minusY,UnitConv.mm2px(100,Res),UnitConv.mm2px(100,Res));
			}
		    sp.setViewportView(bP);
			frame.repaint();
        }
    });
    bH.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.bH = Integer.parseInt(bH.getText());
		    if(badgePanel.bW > badgePanel.bH)
			{
		    	bP.setPreferredSize(new Dimension(UnitConv.mm2px(badgePanel.bH,Res),UnitConv.mm2px(badgePanel.bW,Res)));
				//bP.setBounds(minusX,minusY,UnitConv.mm2px(badgePanel.bH,Res),UnitConv.mm2px(badgePanel.bW,Res));
				//settingsPanel.setBounds(minusX+UnitConv.mm2px(badgePanel.bH+10,Res),minusY,UnitConv.mm2px(100,Res),UnitConv.mm2px(100,Res));
			}
			else
			{
				bP.setPreferredSize(new Dimension(UnitConv.mm2px(badgePanel.bW,Res),UnitConv.mm2px(badgePanel.bH,Res)));
				//bP.setBounds(minusX,minusY,UnitConv.mm2px(badgePanel.bW,Res),UnitConv.mm2px(badgePanel.bH,Res));
				//settingsPanel.setBounds(minusX+UnitConv.mm2px(badgePanel.bW+10,Res),minusY,UnitConv.mm2px(100,Res),UnitConv.mm2px(100,Res));
			}
		    sp.setViewportView(bP);
			frame.repaint();
        }
    });
    Font1.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.Font_F = Integer.parseInt(Font1.getText());
			bP.repaint();
        }
    });
    Font2.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.Font_IO = Integer.parseInt(Font2.getText());
			bP.repaint();
        }
    });
    Font3.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.Font_CITY = Integer.parseInt(Font3.getText());
			bP.repaint();
        }
    });
    F_top.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.F_top = UnitConv.mm2px(Integer.parseInt(F_top.getText()),Res);
			bP.repaint();
        }
    });
    I_top.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.I_top = UnitConv.mm2px(Integer.parseInt(I_top.getText()),Res);
			bP.repaint();
        }
    });
    O_top.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.O_top = UnitConv.mm2px(Integer.parseInt(O_top.getText()),Res);
			bP.repaint();
        }
    });
    CITY_top.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.CITY_top = UnitConv.mm2px(Integer.parseInt(CITY_top.getText()),Res);
			bP.repaint();
        }
    });
    brcdY.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.brcd_y = UnitConv.mm2px(Integer.parseInt(brcdY.getText()),Res);
			bP.repaint();
        }
    });
    brcdW.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.brcd_width = UnitConv.mm2px(Integer.parseInt(brcdW.getText()),Res);
			bP.repaint();
        }
    });
    brcdH.addKeyListener(new KeyAdapter() {
		public void keyReleased(KeyEvent e) {
			String text = "BadgeWidth="+bW.getText()+System.getProperty("line.separator")+"BadgeHeight="+bH.getText()+System.getProperty("line.separator")+"Font1="+Font1.getText()+System.getProperty("line.separator")+"Font2="+Font2.getText()+System.getProperty("line.separator")+"Font3="+Font3.getText()+System.getProperty("line.separator")+"F_Margin="+F_top.getText()+System.getProperty("line.separator")+"I_Margin="+I_top.getText()+System.getProperty("line.separator")+"O_Margin="+O_top.getText()+System.getProperty("line.separator")+"CITY_Margin="+CITY_top.getText()+System.getProperty("line.separator")+"BARCODE_Margin="+brcdY.getText()+System.getProperty("line.separator")+"BARCODE_W="+brcdW.getText()+System.getProperty("line.separator")+"BARCODE_H="+brcdH.getText();
			write("Parameters.txt",text);
			badgePanel.brcd_height = UnitConv.mm2px(Integer.parseInt(brcdH.getText()),Res);
			bP.repaint();
        }
    });
  }
}
class badgePanel extends JPanel {
    /**
	 * 
	 */
	int Res = 100;
	static int bW = 100;
	static int bH = 100;
	static int Font_IO = 18;
	static int Font_F = 18;
	static int Font_CITY = 14;
	String F = "����������������";
	static int F_left = 0;
	static int F_top = 0;
	String I = "����������";
	static int I_left = 0;
	static int I_top = 0;
	String O = "��������������";
	static int O_left = 0;
	static int O_top = 0;
	String CITY = "�����������������";
	static int CITY_left = 0;
	static int CITY_top = 0;
	Rectangle BARCODE = new Rectangle();
	static int brcd_x = 0;
	static int brcd_y = 0;
	static int brcd_width = 0;
	static int brcd_height = 0;
	
	private static final long serialVersionUID = 1L;
	
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		FontMetrics metricsF1 = g.getFontMetrics(new Font("Arial",Font.PLAIN,Font_F));
		FontMetrics metricsF2 = g.getFontMetrics(new Font("Arial",Font.PLAIN,Font_IO));
		FontMetrics metricsF3 = g.getFontMetrics(new Font("Arial",Font.PLAIN,Font_CITY));
		if(bW > bH)
		{
			g.clearRect(0,0,getWidth(),getHeight());
			g.drawRect(0,0,UnitConv.mm2px(bH,Res),UnitConv.mm2px(bW,Res));
			g.translate(UnitConv.mm2px(bH,Res), 0);
			((Graphics2D) g).rotate(Math.PI/2);
			g.setFont(new Font("Arial",Font.PLAIN,Font_F));
	        F_left  = UnitConv.mm2px(bW/2,Res)+(UnitConv.mm2px(bW/2,Res) - metricsF1.stringWidth(F)) / 2;
	        g.drawString(F, F_left, F_top);
	        g.setFont(new Font("Arial",Font.PLAIN,Font_IO));
	        I_left  = UnitConv.mm2px(bW/2,Res)+(UnitConv.mm2px(bW/2,Res) - metricsF2.stringWidth(I)) / 2;
	        g.drawString(I, I_left, I_top);
	        O_left  = UnitConv.mm2px(bW/2,Res)+(UnitConv.mm2px(bW/2,Res) - metricsF2.stringWidth(O)) / 2;
	        g.drawString(O, O_left, O_top);
	        g.setFont(new Font("Arial",Font.PLAIN,Font_CITY));
	        CITY_left  = UnitConv.mm2px(bW/2,Res)+(UnitConv.mm2px(bW/2,Res) - metricsF3.stringWidth(CITY)) / 2;
	        g.drawString(CITY, CITY_left, CITY_top);
	        g.drawRect(UnitConv.mm2px(3*bW/4,Res)-brcd_width/2, brcd_y, brcd_width, brcd_height);
			
		}
		else
		{
			g.clearRect(0,0,getWidth(),getHeight());
			g.drawRect(0,0,UnitConv.mm2px(bW,Res),UnitConv.mm2px(bH,Res));
	        g.setFont(new Font("Arial",Font.PLAIN,Font_F));
	        F_left  = (UnitConv.mm2px(bW,Res) - metricsF1.stringWidth(F)) / 2;
	        g.drawString(F, F_left, F_top);
	        g.setFont(new Font("Arial",Font.PLAIN,Font_IO));
	        I_left  = (UnitConv.mm2px(bW,Res) - metricsF2.stringWidth(I)) / 2;
	        g.drawString(I, I_left, I_top);
	        O_left  = (UnitConv.mm2px(bW,Res) - metricsF2.stringWidth(O)) / 2;
	        g.drawString(O, O_left, O_top);
	        g.setFont(new Font("Arial",Font.PLAIN,Font_CITY));
	        CITY_left  = (UnitConv.mm2px(bW,Res) - metricsF3.stringWidth(CITY)) / 2;
	        g.drawString(CITY, CITY_left, CITY_top);
	        g.drawRect(UnitConv.mm2px(bW/2,Res)-brcd_width/2, brcd_y, brcd_width, brcd_height);
		}
    }
}