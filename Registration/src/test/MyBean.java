package test;

import javax.swing.JRadioButton;

public class MyBean {
	 
    private String name;
    private String city;
    private String email;
    private String membership;
    private String order;

    public MyBean(String name,String city,String email,String membership,String order) {
    	this.name = name;
    	this.city = city;
    	this.email = email;
    	this.membership = membership;
    	this.order = order;
    }

    public String getName() {
        return name;
    }
    public String getCity() {
        return city; 
    }
    public String getEmail() {
        return email;
    }
    public String getMembership() {
        return membership;
    }
    public String getOrder() {
        return order;
    }
}

