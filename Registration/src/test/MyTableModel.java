package test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

public class MyTableModel extends AbstractTableModel {
	 
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private List<MyBean> beans;
    
    public MyTableModel(List<MyBean> beans) {
        this.beans = beans;
    }
    @Override
    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }
    @Override
    public Class<?> getColumnClass(int columnIndex) {
    	switch (columnIndex) {
        case 0:
            return String.class;
        case 1:
            return String.class;
        case 2:
            return String.class;
        case 3:
            return String.class;
        case 4:
            return String.class;    
        default: return Object.class;    
        }
        
    }
    @Override
    public int getColumnCount() {
        return 5;
    }
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return "���";
        case 1:
            return "�����";
        case 2:
            return "E-mail";
        case 3:
            return "��������";
        case 4:
            return "������� ��������";        
        }
        return "";
    }
    @Override
    public int getRowCount() {
        return beans.size();
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        MyBean bean = beans.get(rowIndex);
        switch (columnIndex) {
        case 0:
            return bean.getName();
        case 1:
            return bean.getCity();
        case 2:
            return bean.getEmail();
        case 3:
            return bean.getMembership();
        case 4:
            return bean.getOrder();
        }
        return "";
    }
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    @Override
    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }
    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {

    }

}
