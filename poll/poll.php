<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>
<?
CModule::IncludeModule('iblock');
if($_REQUEST["IsActiveVoting"] == "Y")
{
	$ActiveSections = Array();
	$obSections = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>false,"ACTIVE"=>"Y"),false,Array(),false);
	while($arSections = $obSections->GetNext())
	{
		$obInsideSections =  CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"ACTIVE"=>"Y","SECTION_ID"=>$arSections["ID"]),false,Array(),false);
		$arInsideSections = $obInsideSections->GetNext();
		$ActiveSections[] = Array($arSections["~NAME"],$arInsideSections["ID"]);
	}
	echo json_encode($ActiveSections);
}
if($_REQUEST["HALL_NAME"])
{
$obSections = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"ACTIVE"=>"Y","NAME"=>$_REQUEST["HALL_NAME"]),false,Array(),false);
if($arSections = $obSections->GetNext())
{
	$obInsideSections =  CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"ACTIVE"=>"Y","SECTION_ID"=>$arSections["ID"]),false,Array(),false);
	$arInsideSections = $obInsideSections->GetNext();
	$SectionID = $arInsideSections["ID"];
}
else
{
	$bs = new CIBlockSection;
	$arFields = Array(
	  "ACTIVE" => "Y",
	  "IBLOCK_ID" => 74,
	  "NAME" => strip_tags($_REQUEST["HALL_NAME"]),
	  );
	  $SectionID = $bs->Add($arFields);
}
}
//проверка активных разделов
if($_REQUEST["IsVoting"] == "Y" && $_REQUEST["Sess_id"] && $_REQUEST["HALL"])
{
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$_REQUEST["HALL"],"ACTIVE"=>"Y"),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obIsVoting = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>$arHall["ID"],"ACTIVE"=>"Y"),false,Array("ID","NAME","IBLOCK_ID","UF_*"),false);
	if($arIsVoting = $obIsVoting->GetNext())
	{
		$obAlreadyVote = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arIsVoting["ID"]),"PROPERTY_SESSION_ID"=>$_REQUEST["Sess_id"]),false,false,Array());
		if($arAlreadyVote = $obAlreadyVote->GetNext())
		{
			$AlreadyVote = 1;
			$AlreadyVoteName = $arAlreadyVote["~NAME"];
		}
		else
		{
			$AlreadyVote = 0;
			$AlreadyVoteName = "";	
		}
		$countVotes = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arIsVoting["ID"])),Array(),false,Array());
		echo json_encode(Array($countVotes,$AlreadyVote,$AlreadyVoteName,$arIsVoting["UF_BUTTONS"],$arIsVoting["UF_NAME"],$arIsVoting["UF_VARS"],$arIsVoting["UF_TIMER"],$arIsVoting["UF_TIMEFROMSTART"]));
	}
	else
		echo "N";
}
//получение результатов последнего голосования
if($_REQUEST["GiveMeID"] == "Y")
{
	echo '?????';
	$GlobalResults = Array();
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$_REQUEST["HALL"],"ACTIVE"=>"Y"),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obSections2 = CIBlockSection::GetList(Array("ID"=>"DESC"),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"]),"UF_DEACTIVATE"=>false),false,Array("ID","IBLOCK_ID","NAME","UF_DEACTIVATE","UF_BUTTONS","UF_NAME"),false);
	while($arSections2 = $obSections2->GetNext())
	{
	if(!$arSections2["UF_DEACTIVATE"])
	{
	$obResults = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arSections2["ID"])),false,false,Array());
	$Results = Array();
	while($arResults = $obResults->GetNext())
	{
		$Results[] = $arResults["~NAME"];
	}
	$GlobalResults[]=Array($Results,$arSections2["UF_BUTTONS"],$arSections2["UF_NAME"]);
	}
	}
	echo json_encode($GlobalResults);
}
//создание раздела голосования
function newPoll($Name,$N_buttons, $Vars)
{
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$Name,"ACTIVE"=>"Y"),false,Array(),false);
	$arHall = $obHall->GetNext();
	$countSections = CIBlockSection::GetCount(Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"])));
	$poll = new CIBlockSection;
	$namePoll = "Опрос №".($countSections+1);
	$arFieldsSection = Array(
										"ACTIVE" => "Y",
										"IBLOCK_SECTION_ID"=>$arHall["ID"],
										"IBLOCK_ID" => 74,
										"NAME" => $_REQUEST["Q_NAME"]?$_REQUEST["Q_NAME"]:$namePoll,
										"UF_TIMER"=>intval($_REQUEST["TIMER"]),
										"UF_BUTTONS"=>$N_buttons,
										"UF_NAME"=>$_REQUEST["Q_NAME"]?$_REQUEST["Q_NAME"]:$namePoll,
										"UF_VARS"=>json_decode($Vars),
										"UF_TIMEFROMSTART"=>intval($_REQUEST["TIMEFROMSTART"])
	);
	$IDpoll = $poll->Add($arFieldsSection);
}
if($_REQUEST["Start"] == "Y" && $_REQUEST["HALL"]  && $_REQUEST["N_BUTTONS"] && $_REQUEST["VARS"])
{
	newPoll($_REQUEST["HALL"],$_REQUEST["N_BUTTONS"], $_REQUEST["VARS"]);
}
//создание элементов инфоблока
function newElement($nameElement,$Name)
{
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$Name,"ACTIVE"=>"Y"),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obSections = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"]),"ACTIVE"=>"Y"),false,Array(),false);
	$arSections = $obSections->GetNext();
	$element = new CIBlockElement;	
	
	$arFieldsElement = Array(
										"ACTIVE" => "Y",
										"IBLOCK_ID" => 74,
										"IBLOCK_SECTION_ID" => $arSections["ID"],
										"NAME" => $nameElement,
										"PROPERTY_VALUES"=> Array("SESSION_ID"=>$_REQUEST["Sess_id"])
	);
	$IDElement = $element->Add($arFieldsElement);
}
if($_REQUEST["Vote"] && $_REQUEST["Sess_id"] && $_REQUEST["HALL"])
{
	newElement($_REQUEST["Vote"],$_REQUEST["HALL"]);
}
//прекращение голосования - деактивация раздела
function closePoll($Name)
{
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$Name,"ACTIVE"=>"Y"),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obSections = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"]),"ACTIVE"=>"Y"),false,Array(),false);
	$arSections = $obSections->GetNext();
	$poll = new CIBlockSection;
	
	$arFieldsSection = Array(
										"ACTIVE" => "N",
										"IBLOCK_ID" => 74,
	);
	$resUpdate = $poll->Update($arSections["ID"],$arFieldsSection);
	echo $IDpoll;
}
if($_REQUEST["Close"] == "Y" && $_REQUEST["HALL"])
{
	closePoll($_REQUEST["HALL"]);
}
// деактивация опроса
if($_REQUEST["ToDeact"] == "Y" && $_REQUEST["HALL"])
{
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$_REQUEST["HALL"]),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obSections = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"])),false,Array(),false);
	while($arSections = $obSections->GetNext())
	{
	if($arSections["ID"])
	{
	$bs = new CIBlockSection;
	$arFields = Array(
		"UF_DEACTIVATE"=>"Y"
	);
	$res = $bs->Update($arSections["ID"], $arFields);
	}
	}
}
// все голосования из зала
if($_REQUEST["ShowVotings"] == "Y" && $_REQUEST["HALL"])
{
	$Votings = Array();
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$_REQUEST["HALL"]),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obSections = CIBlockSection::GetList(Array("ID"=>"DESC"),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"])),false,Array("ID","IBLOCK_ID","NAME","UF_*"),false);
	while($arSections = $obSections->GetNext())
	{
		$Votings[] = Array($arSections["~NAME"],$arSections["UF_DEACTIVATE"],$arSections["UF_BUTTONS"],$arSections["ID"]);
	}
	echo json_encode($Votings);
}
// снова активация результатов
if($_REQUEST["ActivateAgain"] == "Y" && $_REQUEST["HALL"] && $_REQUEST["SECTION"])
{
	$Sections = array_diff(json_decode($_REQUEST["SECTION"]),Array(''));
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$_REQUEST["HALL"]),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obSections = CIBlockSection::GetList(Array("ID"=>"DESC"),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"])),false,Array(),false);
	while($arSections = $obSections->GetNext())
	{
		if($arSections["ID"])
		{
			$bs = new CIBlockSection;
			$arFields = Array(
				"UF_DEACTIVATE"=>"Y"
			);
			$res = $bs->Update($arSections["ID"], $arFields);
		}
	}
	foreach($Sections as $index=>$ID)
	{
	$obSections = CIBlockSection::GetList(Array("ID"=>"DESC"),Array("IBLOCK_ID"=>74,"ID"=>$ID),false,Array(),false);
	while($arSections = $obSections->GetNext())
	{
		if($arSections["ID"])
		{
			$bs = new CIBlockSection;
			$arFields = Array(
				"UF_DEACTIVATE"=>""
			);
			$res = $bs->Update($arSections["ID"], $arFields);
		}
	}
	}
}
// прибавка времени от начала голосования
if($_REQUEST["PlusTimeFromStart"] == "Y" && $_REQUEST["HALL"] && $_REQUEST["TIMEPLUS"])
{
	$obHall = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>74,"NAME"=>$_REQUEST["HALL"]),false,Array(),false);
	$arHall = $obHall->GetNext();
	$obSections = CIBlockSection::GetList(Array("ID"=>"DESC"),Array("IBLOCK_ID"=>74,"SECTION_ID"=>Array($arHall["ID"]),"ACTIVE"=>"Y"),false,Array("ID","IBLOCK_ID","NAME","UF_*"),false);
	while($arSections = $obSections->GetNext())
	{
			$bs = new CIBlockSection;
			$arFields = Array(
				"UF_TIMEFROMSTART"=>intval($_REQUEST["TIMEPLUS"])
			);
			$res = $bs->Update($arSections["ID"], $arFields);
	}
}
?>