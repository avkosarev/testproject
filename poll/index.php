<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Голосование");
?>
<? //if(!$_REQUEST["H"] && !in_array(208,CUser::GetUserGroup($USER->GetID()))) die();?>
<script>
	var Answer = 0;
	var AllVotings = new Array();
	var ShowLegend = 'block';
	var Active_Votings = new Array();
	var Vote_Now ='';
	var HALL_NAME;
	var Q_name;
	var N_buttons;
	var JSONResults;
	var Vars;
	var Results = new Array();
	var Show = true;
	var timerHalls;
	var timerSections;
	var timePlus;	
	var Colors = ["red","orange","yellow","green","lightskyblue","blue","violet","gray","brown","black","lightgray","cyan"];
	timerHalls = setInterval(function() {
	  CheckActiveVoting();
	}, 2000);
	function CheckActiveVoting() {
			document.getElementById('footer').style.display='none'; 
			document.getElementById('voting').style.display='none'; 
			document.getElementById('halls').style.display='block'; 
			document.getElementById('preview').style.display='none';
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var ActiveSections = JSON.parse(this.responseText);
					for(var i=0; i< ActiveSections.length; i++)
					{
						if(ActiveSections[i][1])
							document.getElementById(ActiveSections[i][0]).innerHTML = document.getElementById(ActiveSections[i][0]).id.replace("Зал","Зал\\Hall") + " <span style='color:orange;' class='glyphicon glyphicon-fire'></span>";
						else
							document.getElementById(ActiveSections[i][0]).innerHTML = document.getElementById(ActiveSections[i][0]).id.replace("Зал","Зал\\Hall");
					}
				}
			}
			xmlhttp.open("GET", "poll.php?IsActiveVoting=Y", true);
			xmlhttp.send();
	}
	function CreateHall(value) {
			//var xmlhttp = new XMLHttpRequest();
			//xmlhttp.open("GET", "poll.php?HALL_NAME="+value, true);
			//xmlhttp.send();
	}
	function IsVoting() {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var SPLITResponseData = this.responseText.split('?????');
					if(SPLITResponseData[0].replace(/\s/g, '') == "N")
					{
						document.getElementById('sub').innerHTML = 'Подтвердить \\ Confirm'; 
						document.getElementById('sub').disabled=false; 
						for(var i=0; i<document.getElementsByClassName('button_question').length; i++) { document.getElementsByClassName('button_question')[i].disabled=false;}
						document.getElementById("voting").style.display = "none";
						<? if(in_array(208,CUser::GetUserGroup($USER->GetID()))):?>
						document.getElementById("start_voting").style.display = "inline-block";
						document.getElementById("close_voting").style.display = "none";
						document.getElementById("deact_voting").style.display = "inline-block";
						document.getElementById('q_name').style.display='inline-block';
						document.getElementById('n_buttons').style.display='inline-block';
						document.getElementById("vars").style.display = 'block';
						document.getElementById("time").style.display = 'inline-block';
						document.getElementById("span_last_vote").style.display = 'block';
						<?endif;?>
						Answer = 0;
						if(SPLITResponseData[1].length > 2)
						{	
						JSONResults = JSON.parse(SPLITResponseData[1]);
						var freqData=[];
						var maxValues = new Array();
						var maxValuesInArray = new Array();
						var MAX = parseInt(JSONResults[0][1]);
						for(var i=0; i<JSONResults.length; i++)
						{
							if(parseInt(JSONResults[i][1]) > MAX)
								MAX = parseInt(JSONResults[i][1]);
						}
						for(var X=0;X<JSONResults.length;X++)
						{		
						Results = new Array();
						for(var i=0; i<JSONResults[X][0].length; i++)
						{
							if(typeof Results[parseInt(JSONResults[X][0][i])-1] == 'undefined')
							Results[parseInt(JSONResults[X][0][i])-1]=0;
							Results[parseInt(JSONResults[X][0][i])-1]++;	
						}
							var freqData_local=[];
							for(var i=0; i<MAX; i++)
							{
								if(Results[i] === undefined)
									Results[i] = 0;
								

									var obj = new Object();
									obj["State"] = "№"+(i+1);
									var vars = new Object();
									for(var j=0;j<MAX;j++)
									{
										if(i==j)
											vars["No"+(j+1)] = parseInt(Results[i]);
										else
											vars["No"+(j+1)] = 0;
									}
									obj["freq"] = vars;
									freqData_local.push(obj);
							}
							maxValues.push(Math.max.apply(null,Results));
							freqData.push(freqData_local);
							document.getElementById("preview").style.display = "block";
						}
						var maxVal = Math.max.apply(null,maxValues);
						for(var s=0; s<maxValues.length;s++)
						{
							if(maxVal == maxValues[s])
							{
								maxValuesInArray.push(s);
							}
						}
						var listVotes = '<h4><div id="votings_list" style="display:'+ShowLegend+';"><ul  style="margin: 5px; padding: 5px; background:rgba(0,0,0,0.1); border-radius:10px;">';
						var step = 0;
						for(var s=0; s<AllVotings.length; s++)
						{
							if(AllVotings[s][0] !== undefined && AllVotings[s][1] ===null)
							{
								listVotes+='<li style="list-style-type: none;"><span id="leg'+step+'" style="padding: 5px; color:white;">&#9873;</span>'+AllVotings[s][0]+'</li>';
								step++;
							}
						}
						listVotes+='</ul></div></h4>';
						document.getElementById('preview').innerHTML = '<center><h2 class="hidden-xs">Результаты опроса \\ Voting results</h2></center>'+listVotes;
						if(ShowLegend == 'block')
						{
							$("#votings_list").fadeIn('slow');
						}
						else
						{
							$("#votings_list").fadeOut('slow');
						}
						/////////DASHBOARD///////////////
						var hG={},    hGDim = {t: 50, r: 0, b: 30, l: 0};
						hGDim.w = document.getElementById('preview').offsetWidth, 
						hGDim.h = 300 - hGDim.t - hGDim.b;
						//create svg for histogram.
						var hGsvg = d3.select("#preview").append("svg")
							.attr("width", hGDim.w + hGDim.l + hGDim.r)
							.attr("height", hGDim.h + hGDim.t + hGDim.b).append("g")
							.attr("transform", "translate(" + hGDim.l + "," + hGDim.t + ")");
							var Offset = 0;
							for(var w=0;w<freqData.length;w++)
							{	
							freqData[w].forEach(function(d){d.total=0; for(var i=0; i< MAX; i++) d.total+=d.freq["No"+(i+1)];});
							// create function for x-axis mapping.
							var fD = freqData[w].map(function(d){return [d.State,d.total];});
							var x = d3.scale.ordinal().rangeRoundBands([0, hGDim.w], 0.2)
									.domain(fD.map(function(d) { return d[0]; }));
							// Add x-axis to the histogram svg.
							hGsvg.append("g").attr("class", "x axis")
								.attr("transform", "translate(0," + hGDim.h + ")")
								.call(d3.svg.axis().scale(x).orient("bottom"));
							if(freqData.length == 1)
								var WidthOfEl = x.rangeBand()/(2*freqData.length);
							else
								var WidthOfEl = x.rangeBand()/(freqData.length);
							// Create function for y-axis map.
							var y = d3.scale.linear().range([hGDim.h, 0])
									.domain([0, d3.max(fD, function(d) { return d[1]; })]);

							// Create bars for histogram to contain rectangles and freq labels.
							var bars = hGsvg.selectAll(".bar").data(fD).enter()
									.append("g").attr("class", "bar"+w);		
							//create the rectangles.
							if(document.getElementById('leg'+w))
							document.getElementById('leg'+w).style.color=Colors[w];	
							bars.append("rect")
								.attr("height", function(d) { if(in_array(w,maxValuesInArray))return hGDim.h - y(d[1]); else return d[1]*(hGDim.h - y(d[1]))/maxVal;})
								.attr("x", function(d) { return x(d[0])+Offset; })
								.attr("y", function(d) { if(in_array(w,maxValuesInArray)) return y(d[1]); else return hGDim.h-this.height.baseVal["value"];})
								.attr("rx","5")
								.attr("width", x.rangeBand()/freqData.length)
								.attr('fill',Colors[w])
							//Create the frequency labels above the rectangles.
							bars.append("text").text(function(d){ return d3.format(",")(d[1]);})
								.attr("x", function(d) { return x(d[0])+Offset+x.rangeBand()/(2*freqData.length); })
								.attr("y", function(d) { if(in_array(w,maxValuesInArray)) return y(d[1])-5; else return hGDim.h-this.parentNode.getElementsByTagName("rect")[0].height.baseVal["value"]-5;})
								.attr("text-anchor", "middle")
								.attr("style", "font-size: "+WidthOfEl/5+"px; font-weight:bold");
							Offset+=(x.rangeBand()/freqData.length);
							var Texts = document.getElementsByClassName('bar'+w);
							for(var i=0;i<Texts.length;i++)
							{
								var Total=0;
								for(var lol =0; lol < freqData[w].length; lol++)
									Total+=freqData[w][lol].total;
								if(Total == 0) Total = 1;
								Texts[i].getElementsByTagName('text')[0].innerHTML += "<tspan class='hidden-xs'> ("+((parseInt(Texts[i].getElementsByTagName('text')[0].innerHTML)/Total)*100).toFixed(1)+"%)</tspan>";
							}		
							}						
						}
						else
						{
							document.getElementById("preview").style.display = "block";
							document.getElementById('preview').innerHTML = '<center><h2>Нет активного опроса \\ No active voting</h2><h2>Для участия в голосовании с помощью смартфона или компьютера перейдите по ссылке <a style="text-decoration: underline ;" href="/poll/">racvs.ru/poll/</a> или отсканируйте QR-код</h2><img src="img/QR.jpg" /></center>';
						}
						for(var i=0; i<document.getElementsByClassName('button_question').length; i++) 
						{ 
							document.getElementsByClassName('button_question')[i].className = document.getElementsByClassName('button_question')[i].className.replace('btn-success','btn-primary');
						}
						Show = true;	
					}
					else
					{
						var ResponseData = JSON.parse(SPLITResponseData[0]);
						var inner='';
						var count = 0;
						document.getElementById("voting").style.display = "block";
						if(Show)
						{
						document.getElementById("voting").innerHTML = "<div class='row'><center><h2><b>"+ResponseData[4]+"</b>"+"</h2></center><div class='row questions'><div class='col-xs-12 hidden-xs'><hr/><h2>Варианты ответов \\ Answers variants</h2><hr/></div></div><div id='progress' class='progress' style='margin:10px; display:none;'><div class='progress-bar' id='progressbar' role='progressbar' aria-valuenow='70' aria-valuemin='0' aria-valuemax='100' style='width:0%'></div></div>";	
						var answers_variants = ResponseData[5];
						if(answers_variants.length > 0)
						{
						var answer_variantsHTML = "<div style='padding: 5px; margin: 5px; background: rgba(0,255,0,0.1); border-radius: 10px; border: 1px solid green;' class='row hidden-xs'><h3><ol>";
						for(var i=0; i< answers_variants.length;i++)
						{
							answer_variantsHTML+="<li>"+answers_variants[i]+"</li>";
						}
						answer_variantsHTML+="</ol></h3></div>";
						document.getElementById("voting").innerHTML+=answer_variantsHTML;
						}
						for(var i=0; i<parseInt(ResponseData[3]);i++)
						{
							if(count%2 == 0)
							inner+="<div class='row questions'>";
							inner+="<div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>";
							inner+="<button id='"+(i+1)+"' style='width:100%;' type='button' onclick='document.getElementById(\"sub\").disabled=false; Answer = this.innerHTML; for(var i=0; i<document.getElementsByClassName(\"button_question\").length; i++) { document.getElementsByClassName(\"button_question\")[i].className = document.getElementsByClassName(\"button_question\")[i].className.replace(\"btn-success\",\"btn-primary\"); } this.className = this.className.replace(\"btn-primary\",\"btn-success\");' class='btn btn-lg btn-primary button_question'>"+(i+1)+"</button>";
							inner+="</div>";
							count++;
							if(count == 2)
							{
								inner+="</div>";
								count=0;
							}
						}
						document.getElementById("voting").innerHTML+=inner;
						document.getElementById("voting").innerHTML+="<div class='row questions'><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'><button id='sub' style='width:100%;' disabled='true' type='button' class='btn btn-lg btn-danger button_submit' onclick='SendVote(); this.disabled=true; for(var i=0; i<document.getElementsByClassName(\"button_question\").length; i++) { document.getElementsByClassName(\"button_question\")[i].disabled=true;}'>Подтвердить \\ Confirm</button></div></div>";
						document.getElementById("voting").innerHTML+="<div id='vote_results' class='alert alert-info'><h2>Проголосвали \\ Voted:</h2></div>";
						<? if($_REQUEST["B"]):?>
						document.getElementById("<?=$_REQUEST["B"];?>").click();
						<?endif;?>
						<? if($_REQUEST["SUB"]):?>
						document.getElementById("<?=$_REQUEST["SUB"];?>").click();
						<?endif;?>
						Show = false;
						}
						if(parseInt(ResponseData[6]) > 0)
						{
							document.getElementById('progress').style.display='block';
							document.getElementById('progressbar').style.width=parseInt((parseInt(ResponseData[7])/parseInt(ResponseData[6]))*100)+"%";
						}
						else
						{
							document.getElementById('progress').style.display='none';
							document.getElementById('progressbar').style.width="0%";
						}
						document.getElementById("preview").style.display = "none";
						if(ResponseData[1] != 0)
						{
							for(var i=0; i<document.getElementsByClassName('button_question').length; i++) { document.getElementsByClassName('button_question')[i].disabled=true;}
							document.getElementById(ResponseData[2]).className=document.getElementById(ResponseData[2]).className.replace('btn-primary','btn-success');
							document.getElementById('sub').disabled = true;
							document.getElementById('sub').innerHTML = "Ваш голос учтен \\ You voted";
						}
						document.getElementById("vote_results").innerHTML = "<h2>Проголосовали \\ Voted: " + ResponseData[0] + "</h2>";
						<? if(in_array(208,CUser::GetUserGroup($USER->GetID()))):?>
						document.getElementById("start_voting").style.display = "none";
						document.getElementById("close_voting").style.display = "block";
						document.getElementById('deact_voting').style.display='none';
						document.getElementById('q_name').style.display='none';
						document.getElementById('n_buttons').style.display='none';
						document.getElementById("vars").style.display = 'none';
						document.getElementById("time").style.display = 'none';
						document.getElementById("show_results").style.display = 'none';
						document.getElementById("span_last_vote").style.display = 'none';
						<? endif;?>
					}
				}
			};
			xmlhttp.open("GET", "poll.php?GiveMeID=Y&IsVoting=Y"+"&Sess_id="+"<?=$_SESSION["SESS_IP"];?>&HALL="+HALL_NAME, true);
			xmlhttp.send();
	}
	function SendVote() {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET", "poll.php?Vote="+Answer+"&Sess_id="+"<?=$_SESSION["SESS_IP"];?>&HALL="+HALL_NAME, true);
			xmlhttp.send();
	}
	function ToMakeDeact() {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET", "poll.php?ToDeact=Y&HALL="+HALL_NAME, true);
			xmlhttp.send();
	}
	function StartVoting() {
			document.getElementById("vars").innerHTML = '';
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET", "poll.php?Start=Y&HALL="+HALL_NAME+"&Q_NAME="+Q_name+"&N_BUTTONS="+N_buttons+"&VARS="+JSON.stringify(Vars)+"&TIMER="+document.getElementById('time').value+"&TIMEFROMSTART="+timePlus, true);
			xmlhttp.send();
	}		
	function CloseVoting() {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET", "poll.php?Close=Y&HALL="+HALL_NAME, true);
			xmlhttp.send();
	}
	function ShowVotings() {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					AllVotings = JSON.parse(this.responseText);
					Active_Votings = new Array();
					var x = document.getElementById("show_results");
					if(x)
					{
					x.innerHTML = '';
					for(var i=0; i< AllVotings.length; i++)
					{
						var checkbox = document.createElement("input");
						checkbox.id = "checkbox" + i;
						checkbox.type = "checkbox";
						checkbox.value = AllVotings[i][3];
						if(AllVotings[i][1] === null)
							checkbox.checked=true;
						if(checkbox.checked)
							Active_Votings.push(AllVotings[i][3]);
						checkbox.onclick = function(){if(this.checked) Active_Votings.push(this.value); else { delete Active_Votings[Active_Votings.indexOf(this.value)];} Activate(JSON.stringify(Active_Votings));};
						var label = document.createElement('label');
						label.for = "checkbox" + i;
						label.appendChild(checkbox);
						label.appendChild(document.createTextNode(AllVotings[i][0]+" ( Вариантов ответа:"+AllVotings[i][2]+" )"));
						x.appendChild(label);
						var br = document.createElement("br");
						x.appendChild(br);
					}
					}
				}
			}
			xmlhttp.open("GET", "poll.php?ShowVotings=Y&HALL="+HALL_NAME, true);
			xmlhttp.send();
	}
	function Activate(value) {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET", "poll.php?ActivateAgain=Y&HALL="+HALL_NAME+"&SECTION="+value, true);
			xmlhttp.send();
	}
	function PlusTimeFromStart(value) {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("GET", "poll.php?PlusTimeFromStart=Y&HALL="+HALL_NAME+"&TIMEPLUS="+value, true);
			xmlhttp.send();
	}
	function in_array(value, array) 
	{
    for(var i = 0; i < array.length; i++) 
    {
        if(array[i] == value) return true;
    }
    return false;
	}
</script>
    <div id="container" class="container">
    		<div class="row" style="padding-bottom:10px;">
    			<div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
    				<img style="padding-top: 15px;" src="/poll/img/LogoAsso_small.jpg" alt="" class="logo_racvs img-responsive">
    			</div>
    			<div class="col-lg-11 col-md-10 col-sm-10 col-xs-9">
    				<h1 id="head">Опросы \ Voting</h1>
    			</div>
    		</div>
	
		<div id="halls">
			<div class="row">
				<div style="margin-top:5px; margin-bottom: 5px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='Зал №1' style="width:100%;" type="button" onclick="document.getElementById('back').style.visibility='visible'; HALL_NAME = this.id; document.getElementById('head').innerHTML='Опросы \\ Voting &nbsp;&nbsp;&nbsp;<b>'+ HALL_NAME.replace('Зал','Зал\\Hall')+'</b>'; CreateHall(this.id); document.getElementById('halls').style.display='none';  document.getElementById('footer').style.display='block'; clearInterval(timerHalls); ShowVotings();	IsVoting();  timerSections = setInterval(function() { ShowVotings(); IsVoting();}, 2000);" class="btn btn-lg btn-primary">Зал\Hall №1</button>
				</div>
				<div style="margin-top:5px; margin-bottom: 5px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='Зал №2' style="width:100%;" type="button" onclick="document.getElementById('back').style.visibility='visible'; HALL_NAME = this.id; document.getElementById('head').innerHTML='Опросы \\ Voting &nbsp;&nbsp;&nbsp;<b>'+ HALL_NAME.replace('Зал','Зал\\Hall')+'</b>'; CreateHall(this.id); document.getElementById('halls').style.display='none'; document.getElementById('footer').style.display='block'; clearInterval(timerHalls);	ShowVotings(); IsVoting();  timerSections = setInterval(function() { ShowVotings() ;IsVoting();}, 2000);" class="btn btn-lg btn-primary">Зал\Hall №2</button>
				</div>
			</div>
			<div class="row">
				<div style="margin-top:5px; margin-bottom: 5px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='Зал №3' style="width:100%;" type="button" onclick="document.getElementById('back').style.visibility='visible'; HALL_NAME = this.id; document.getElementById('head').innerHTML='Опросы \\ Voting &nbsp;&nbsp;&nbsp;<b>'+ HALL_NAME.replace('Зал','Зал\\Hall')+'</b>'; CreateHall(this.id); document.getElementById('halls').style.display='none'; document.getElementById('footer').style.display='block'; clearInterval(timerHalls);	ShowVotings(); IsVoting();  timerSections = setInterval(function() { ShowVotings(); IsVoting();}, 2000);" class="btn btn-lg btn-primary">Зал\Hall №3</button>
				</div>
				<div style="margin-top:5px; margin-bottom: 5px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='Зал №4' style="width:100%;" type="button" onclick="document.getElementById('back').style.visibility='visible'; HALL_NAME = this.id; document.getElementById('head').innerHTML='Опросы \\ Voting &nbsp;&nbsp;&nbsp;<b>'+ HALL_NAME.replace('Зал','Зал\\Hall')+'</b>'; CreateHall(this.id); document.getElementById('halls').style.display='none'; document.getElementById('footer').style.display='block'; clearInterval(timerHalls); ShowVotings();	IsVoting();  timerSections = setInterval(function() { ShowVotings(); IsVoting();}, 2000);" class="btn btn-lg btn-primary">Зал\Hall №4</button>
				</div>
			</div>	
			<div class="row">	
				<div style="margin-top:5px; margin-bottom: 5px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='Зал №5' style="width:100%;" type="button" onclick="document.getElementById('back').style.visibility='visible'; HALL_NAME = this.id; document.getElementById('head').innerHTML='Опросы \\ Voting &nbsp;&nbsp;&nbsp;<b>'+ HALL_NAME.replace('Зал','Зал\\Hall')+'</b>'; CreateHall(this.id); document.getElementById('halls').style.display='none'; document.getElementById('footer').style.display='block'; clearInterval(timerHalls);	ShowVotings(); IsVoting();  timerSections = setInterval(function() { ShowVotings(); IsVoting();}, 2000);" class="btn btn-lg btn-primary">Зал\Hall №5</button>
				</div>
				<div style="margin-top:5px; margin-bottom: 5px;" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='Зал №6' style="width:100%;" type="button" onclick="document.getElementById('back').style.visibility='visible'; HALL_NAME = this.id; document.getElementById('head').innerHTML='Опросы \\ Voting &nbsp;&nbsp;&nbsp;<b>'+ HALL_NAME.replace('Зал','Зал\\Hall')+'</b>'; CreateHall(this.id); document.getElementById('halls').style.display='none'; document.getElementById('footer').style.display='block'; clearInterval(timerHalls); ShowVotings();	IsVoting();  timerSections = setInterval(function() { ShowVotings(); IsVoting();}, 2000);" class="btn btn-lg btn-primary">Зал\Hall №6</button>
				</div>
			</div>
				<div class="row hidden-xs">
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-0">
					</div>
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
						<div class="alert alert-info" style="text-align: center;">
						  <span style='color:orange;' class='glyphicon glyphicon-fire'></span> - Голосование открыто \ Voting is active
						</div>
					</div>
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-0">
					</div>					
				</div>		
		</div>
		<div id="preview" onclick="if(document.getElementById('votings_list')) {if(document.getElementById('votings_list').style.display == 'block') {ShowLegend = 'none'; $('#votings_list').hide('fast');} else {ShowLegend = 'block'; $('#votings_list').show('fast');}}" style="padding: 5px !important; display:none" class="jumbotron"><h1>Нет активного опроса \ No active voting</h1></div>
		
		<div id="voting"  style="display:none" class="page_content">	

			<div class="row questions">
				<div class="col-xs-12 hidden-xs"><hr/><h1>Варианты ответов \ Answers variants</h1><hr/></div>
			</div>
			<div class="row questions">
				<div class="col-lg-6 col-md-6col-sm-6 col-xs-6">
					<button id='1' style="width:100%;" type="button" onclick="document.getElementById('sub').disabled=false; Answer = this.innerHTML; for(var i=0; i<document.getElementsByClassName('button_question').length; i++) { document.getElementsByClassName('button_question')[i].className = document.getElementsByClassName('button_question')[i].className.replace('btn-success','btn-primary'); } this.className = this.className.replace('btn-primary','btn-success');" class="btn btn-lg btn-primary button_question">1</button>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='2' style="width:100%;" type="button" onclick="document.getElementById('sub').disabled=false; Answer = this.innerHTML; for(var i=0; i<document.getElementsByClassName('button_question').length; i++) { document.getElementsByClassName('button_question')[i].className = document.getElementsByClassName('button_question')[i].className.replace('btn-success','btn-primary'); } this.className = this.className.replace('btn-primary','btn-success');" class="btn btn-lg btn-primary button_question">2</button>
				</div>
			</div>
			<div class="row questions">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='3' style="width:100%;" type="button" onclick="document.getElementById('sub').disabled=false; Answer = this.innerHTML; for(var i=0; i<document.getElementsByClassName('button_question').length; i++) { document.getElementsByClassName('button_question')[i].className = document.getElementsByClassName('button_question')[i].className.replace('btn-success','btn-primary'); } this.className = this.className.replace('btn-primary','btn-success');" class="btn btn-lg btn-primary button_question">3</button>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<button id='4' style="width:100%;" type="button" onclick="document.getElementById('sub').disabled=false; Answer = this.innerHTML; for(var i=0; i<document.getElementsByClassName('button_question').length; i++) { document.getElementsByClassName('button_question')[i].className = document.getElementsByClassName('button_question')[i].className.replace('btn-success','btn-primary'); } this.className = this.className.replace('btn-primary','btn-success');" class="btn btn-lg btn-primary button_question">4</button>
				</div>
			</div>
			<div class="row questions">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<button id="sub" style="width:100%;" disabled="true" type="button" class="btn btn-lg btn-danger button_submit" onclick="SendVote(); this.innerHTML = 'Ваш голос учтен \\ You voted'; this.disabled=true; for(var i=0; i<document.getElementsByClassName('button_question').length; i++) { document.getElementsByClassName('button_question')[i].disabled=true;}">Подтвердить \ Confirm</button>
				</div>
			</div>
			<div id="vote_results" class="alert alert-info">
				<h2>Проголосовали \ Voted:</h2>
			</div>		
		</div>
			<footer id="footer" style="display:none" class="footer">
			<? if(in_array(208,CUser::GetUserGroup($USER->GetID()))):?>
				<div id="span_last_vote"><center><button onclick="$('#show_results').toggle('slow');" style="width:100%" class="btn btn-lg btn-info button_admin">Показать ранние голосвания \ Show last votings</button></center></div>
				<div id="show_results" style="overflow: auto; max-height: 200px; display:none; position:absolute; z-index: 100; padding: 5px; background: gray; color: white; border-radius: 10px; width: 100%;"></div>
				<input type="text" class="form-control" style="margin:5px;" placeholder="Название вопроса \ Question" id="q_name">
				<input type="number" class="form-control" style="margin:5px;" max="999" min="0" placeholder="Время на ответ \ Time to vote" id="time">
				<input type="text" class="form-control" style="border: 1px solid rgba(255,0,0,0.5); margin:5px;" maxlength="2" onkeyup="var inside_vars=''; if(parseInt(this.value)>0){ for(var i=0; i<parseInt(this.value);i++) inside_vars+='<input id=\'var'+(i+1)+'\' placeholder=\'Вариант\\Variant №'+(i+1)+'\' style=\'margin:5px; background:rgba(0,255,0,0.1);\' class=\'form-control\' type=text>'; document.getElementById('vars').innerHTML=inside_vars;} else document.getElementById('vars').innerHTML=inside_vars;" placeholder="Количество вариантов ответа \ Quantity of answer's variants" id="n_buttons">
				<div id="vars"></div>
				<button type="button" id="start_voting" class="btn btn-lg btn-default button_admin" onclick="if(document.getElementById('n_buttons').value.length == 0) alert('Введите количество вариантов ответа'); Q_name = document.getElementById('q_name').value; N_buttons = document.getElementById('n_buttons').value; Vars = new Array(); for(var k=0;k<parseInt(N_buttons);k++){ Vars.push(document.getElementById('var'+(k+1)).value);} timePlus=1; StartVoting(); document.getElementById('q_name').value=''; document.getElementById('n_buttons').value=''; if(document.getElementById('time').value.length>0) {  var timerLeft = setInterval(function(){ timePlus++; PlusTimeFromStart(timePlus);},1000); setTimeout(function(){clearInterval(timerLeft); CloseVoting();  },1000*parseInt(document.getElementById('time').value));}">Начать опрос \ Start voting</button>
				<button type="button" id="close_voting" style="display:none;" onclick="CloseVoting();" class="btn btn-lg btn-default button_admin">Завершить опрос \ Finish voting</button>
				<button type="button" id="deact_voting" onclick="ToMakeDeact();" class="btn btn-lg btn-danger button_admin">Деактивировать опрос \ Deactivate voting</button>
			<? endif;?>	
				<button type="button" id="back" onclick="this.style.visibility='hidden'; clearInterval(timerSections); document.getElementById('head').innerHTML='Опросы \\ Voting'; Active_Votings = new Array(); Show = true; timerHalls = setInterval(function() { CheckActiveVoting();}, 2000); " class="btn btn-lg btn-info button_admin">К выбору зала \ Choose hall</button>
			</footer>
			
    </div>
<? if($_REQUEST["H"]):?>
<script>document.getElementById("Зал №<?=$_REQUEST["H"];?>").click();</script>
<? endif;?>
<? //if(!in_array(208,CUser::GetUserGroup($USER->GetID()))):?>
<script>//document.getElementById("back").style.display='none';</script>
<? //endif;?>		
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>