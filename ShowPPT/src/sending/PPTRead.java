package sending;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class PPTRead extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String file;
	
	public PPTRead(String f)
	{
		file = f;
		Runtime rt = Runtime.getRuntime();
        try {
            if(file.indexOf(".pdf") >= 0)
    		{
            	Desktop.getDesktop().open(new File(ShowPPT.dirSelect+"\\"+file));
    		}
        	else
        	{
        		rt.exec(new String[]{ShowPPT.SFTPPPTPATH,"/s",ShowPPT.dirSelect+"\\"+file});
        	}

        }catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Please, check settings of file!","ERROR",JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Presenation not found");
        }
	}

}