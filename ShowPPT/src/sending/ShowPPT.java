package sending;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimerTask;
import java.util.Timer;

import javax.swing.*;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

public class ShowPPT extends JFrame{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static String SFTPDISKNAME;
    static String SFTPPPTPATH;
    static String RequestPath;
    static String[] Parameters = new String[3];
	public  static JButton  btnOpenDir    = null;
	public  static JButton  showPPT    = null;
	public  static JButton  close    = null;
    public static ArrayList<String> fileSelect = new ArrayList<String>();
    public static ArrayList<String> formats = new ArrayList<String>();
    static String dirSelect ="";
    static JLabel p_title= new JLabel();
    static JLabel speaker = new JLabel();
    public static File DirName = null;
    static  JFileChooser fileChooser = null;
    private JLabel imageLabel = new JLabel(new ImageIcon("logo.png"));
    private JLabel backG = new JLabel(new ImageIcon("back.jpg"));
    static Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    static int width = (int)dim.getWidth();
    static int height = (int)dim.getHeight();
    static JButton Up =null;
    static JButton Clear =null;
    static JButton Session =null;
    static boolean SetDisk = false;
    public static Timer mTimer;
    public static MyTimerTask mMyTimerTask;
 // ���������� ������ � ����������
    public static JPanel contents = new JPanel();
    
    public ShowPPT() {
        super("Presentation viewer");
        
        mTimer = new Timer();
		mMyTimerTask = new MyTimerTask();
		mTimer.schedule(mMyTimerTask, 0, 1000);
		
        fileSelect.removeAll(fileSelect);
        
        BufferedReader cmd = null;
        try {
        	cmd = new BufferedReader(
                        new InputStreamReader(
                            new FileInputStream("cmd.txt"), Charset.forName("UTF-8")));
            String line;
            while ((line = cmd.readLine()) != null) {
            	Runtime.getRuntime().exec(line);
            }
        } catch (IOException e) {
            // log error
        } finally {
            if (cmd != null) {
                try {
                	cmd.close();
                } catch (IOException e) {
                    // log warning
                }
            }
        }
        
        BufferedReader format = null;
        try {
        	format = new BufferedReader(
                        new InputStreamReader(
                            new FileInputStream("formats.txt"), Charset.forName("UTF-8")));
            String line;
            while ((line = format.readLine()) != null) {
            	formats.add(line);
            }
        } catch (IOException e) {
            // log error
        } finally {
            if (format != null) {
                try {
                	format.close();
                } catch (IOException e) {
                    // log warning
                }
            }
        }

        
        BufferedReader reader = null;
        int FileRow = 0;
        try {
            reader = new BufferedReader(
                        new InputStreamReader(
                            new FileInputStream("Settings.txt"), Charset.forName("UTF-8")));
            String line;
            while ((line = reader.readLine()) != null) {

           	 String[] Row = line.split("=");
                Parameters[FileRow++] = Row[1];
            }
        } catch (IOException e) {
            // log error
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // log warning
                }
            }
        }
        
        SFTPDISKNAME=Parameters[0];
        SFTPPPTPATH=Parameters[1];
        RequestPath=Parameters[2];
        
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        setUndecorated(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        contents.setLayout(null);
        ///
        Up = new JButton("Up");
        Clear = new JButton("Clear");
        Session = new JButton("Session");
        close = new JButton();
        // ������ �������� ����������� ���� ��� ������ ����������
        btnOpenDir = new JButton("Select");
        btnOpenDir.setBounds(width-160,10,70,30);
        btnOpenDir.setFont(new Font("Arial", Font.PLAIN, 10));
        btnOpenDir.setBackground(new Color(0,191,255));
        btnOpenDir.setForeground(Color.WHITE);
        // showPPT
        showPPT = new JButton("Presentation");
        showPPT.setBounds(width-250,height-200,200,75);
        showPPT.setFont(new Font("Arial", Font.PLAIN, 20));
        showPPT.setBackground(Color.ORANGE);
        showPPT.setForeground(Color.GRAY);
        //Clear
        Clear.setBounds(width-80,10,70,30);
        Clear.setFont(new Font("Arial", Font.PLAIN, 10));
        Clear.setBackground(new Color(0,191,255));
        Clear.setForeground(Color.WHITE);
        //Session
        Session.setBounds(10,height-100,200,75);
        Session.setFont(new Font("Arial", Font.PLAIN, 20));
        Session.setBackground(new Color(0,191,255));
        Session.setForeground(Color.WHITE);
        //close
        close.setBounds(1,1,10,10);
        close.setBackground(Color.RED);
        close.setForeground(Color.WHITE);
        // �������� ���������� JFileChooser 
        fileChooser = new JFileChooser(new File(SFTPDISKNAME+":\\"));
        ((JPanel) fileChooser.getComponent(0)).remove(0);
        ((JComboBox)((JPanel) fileChooser.getComponent(0)).getComponent(1)).setEnabled(false);
       ((JPanel)fileChooser.getComponent(3)).remove(0);
       ((JPanel)fileChooser.getComponent(3)).remove(1);
       ((JPanel)((JPanel)fileChooser.getComponent(3)).getComponent(1)).add(Up);
        /////
        p_title.setHorizontalAlignment(SwingConstants.CENTER);
        p_title.setBounds(10,0,width-20,height);
        speaker.setBounds(10,height-100-100,width-250-10,100);
        ///////
        imageLabel.setHorizontalAlignment(SwingConstants.LEFT);
        imageLabel.setBounds(10, 10, width,150);
        // ����������� ���������� � �������
        addFileChooserListeners();
        contents.add(btnOpenDir);
        contents.add(Clear);
        contents.add(close);
        contents.add(Session);
        contents.add(imageLabel);
        contents.add(backG).setBounds(0,0,width,height);
        setContentPane(contents);
        contents.setFocusable(true);
        contents.requestFocusInWindow();
        // ����� ���� �� �����
        setSize(width,height);
        setVisible(true);
    }
	private void addFileChooserListeners() {
		contents.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_P)
		                   	System.exit(0);
            }
        });
		btnOpenDir.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        fileChooser.setDialogTitle("Select File");
		        // ����������� ������ - ������ �������
		        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		        int result = fileChooser.showOpenDialog(ShowPPT.this);
		        // ���� ���������� �������, ������� �� � ���������
		        if (result == JFileChooser.APPROVE_OPTION )
		            {
		        	int l=0;
		        	if(fileChooser.getSelectedFile().getAbsolutePath().indexOf("ID") >= 0)
		        	{
		        		for(int q=fileChooser.getSelectedFile().getAbsolutePath().indexOf("ID");q<fileChooser.getSelectedFile().getAbsolutePath().length();q++)
		        		{
		        			if(fileChooser.getSelectedFile().getAbsolutePath().charAt(q) == '\\')
		        			{
		        				l=q-1; 
		        				break;
		        			}
		        			l=q;
		        		}
		        	}
		        	if(fileChooser.getSelectedFile().getAbsolutePath().indexOf("ID") >= 0)
		        	{
		        	fileChooser.setSelectedFile(new File(fileChooser.getSelectedFile().getAbsolutePath().substring(0,l+1)));
		        	DirName = fileChooser.getSelectedFile();
		        	String response = "";
					try {
						response = HttpsClient.testIt("?SESSION="+URLEncoder.encode(DirName.getAbsolutePath().replace(SFTPDISKNAME+":\\", ""),"UTF-8"));
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		        	if(response.length() > 0)
		        	{
			        		String[] resp = response.split("X{5}");
		    		    	p_title.setText(resp[0]);
		    		    	speaker.setText(resp[1]);
		        	}	
			        fileSelect.removeAll(fileSelect);
		        	contents.removeAll();
		        	contents.add(showPPT);
		        	contents.add(btnOpenDir);
		        	contents.add(Clear);
		        	contents.add(close);
		        	contents.add(Session);
		            contents.add(imageLabel);
		            String[] dirList = DirName.list(new FilenameFilter(){

						@Override
						public boolean accept(File dir, String filename) {
							return new File(dir.getAbsolutePath()+ "\\"+filename).isDirectory();
						}});
		            Arrays.sort(dirList,new WindowsExplorerStringComparator());
		        	JButton[] papers = new JButton[dirList.length];
		        	int Padding = 0;
		        	for(int n=0; n<dirList.length; n++)
		        	{
		        		String[] buttonText = dirList[n].split("-");
		        		papers[n] = new JButton(buttonText[buttonText.length-1]);
		        		papers[n].setActionCommand(dirList[n]);
		        		papers[n].setBounds(250+Padding,height - 100,(width-250)/dirList.length-10,75);
		        		papers[n].setFont(new Font("Arial", Font.PLAIN, 20));
		        		papers[n].setBackground(new Color(0,191,255));
		        		papers[n].setForeground(Color.WHITE);
		        		Padding+=(width-250)/dirList.length;
		        		papers[n].addActionListener(new ActionListener() {
		        		    public void actionPerformed(ActionEvent e) {
		        		    	fileSelect.removeAll(fileSelect);
		        		    	for(int n=0; n<dirList.length; n++)
		    		        	{
		        		    		if(papers[n].getActionCommand() == e.getActionCommand())
		        		    			{
		        		    				papers[n].setBackground(Color.ORANGE);
		        		    				papers[n].setForeground(Color.GRAY);
		        		    			}
		        		    		else
		        		    			{
		        		    				papers[n].setBackground(new Color(0,191,255));
		        		    				papers[n].setForeground(Color.WHITE);
		        		    			}
		    		        	}
		        		    	String response="";
								try {
									response = HttpsClient.testIt("?PATH="+URLEncoder.encode((DirName.getAbsolutePath()+"\\"+e.getActionCommand()).replace(SFTPDISKNAME+":\\", ""),"UTF-8"));
								} catch (UnsupportedEncodingException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								if(response.length() > 0)
		        		    	{
		        		    		String[] resp = response.split("X{5}");
			        		    	p_title.setText(resp[0]);
			        		    	speaker.setText(resp[1]);
		        		    	}
		        		    	File pres = new File(DirName.getAbsolutePath()+"\\"+e.getActionCommand());
		        		    	String[] preslist = pres.list(new FilenameFilter(){

									@Override
									public boolean accept(File dir, String name) {
										// TODO Auto-generated method stub
										for(int i=0; i<formats.size();i++)
										{
											if(name.indexOf(formats.get(i)) >=0)
											{
												dirSelect = dir.getAbsolutePath();
												fileSelect.add(name);
												return true;
											}
										}
										return false;
									}});
		        		    	if(preslist.length == 0)
		        		    		fileSelect.removeAll(fileSelect);
		        		    	repaint();
		        		    }
		        		});
		        		contents.add(papers[n]);
		        	}
		        	contents.add(p_title);
		        	contents.add(speaker);
		        	contents.add(backG).setBounds(0,0,width,height);
		            }
		        	else
		        	{
		        		JOptionPane.showMessageDialog(null, "Please, choose session!");
		        		btnOpenDir.doClick();
		        	}
		        	repaint();
		            }
		    }
		});
		showPPT.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	if(fileSelect.size() == 0)
		    		JOptionPane.showMessageDialog(null, "Presentation not found");
		    	else if(fileSelect.size() == 1)
		    		new PPTRead(fileSelect.get(0));
		    	else
		    		{
		    			  String[] buttons = new String[fileSelect.size()];
		    			  for(int n=0; n<fileSelect.size();n++)
		    			  {
		    				  buttons[n] = fileSelect.get(n);
		    			  }
		    			  JOptionPane myOptionPane = new JOptionPane(
		    			  "Current folder contains "+fileSelect.size()+" presentations.",
		    	          JOptionPane.QUESTION_MESSAGE,
		    	          JOptionPane.YES_NO_OPTION, 
		    	          null, 
		    	          buttons);
			    	      JDialog myDialog = myOptionPane.createDialog(null, "Select presentation");
			    	      myDialog.setModal(true);
			    	      myDialog.setVisible(true);
			    	      String result = (String) myOptionPane.getValue();
			    	      // Note: result might be null if the option is cancelled
			    	      if(result != null)
			    	      {
			    	    	  new PPTRead(result.toString());
			    	      }

		    		}
		    }
		});
		Up.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	String Path = fileChooser.getCurrentDirectory().getAbsolutePath();
		    		if(Path.charAt(Path.length()-1) != '\\' && Path.charAt(Path.length()-2) != ':')
		    		{
				    	int l=Path.length()-1;
				    	while(Path.charAt(l) != '\\')
				    	{
				    		l--;
				    	}
				    	fileChooser.setCurrentDirectory(new File(Path.substring(0,l+1)));
		    		}
		    	
		    }
		});
		Clear.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	contents.removeAll();
	        	contents.add(btnOpenDir);
	        	contents.add(Clear);
	        	contents.add(close);
	        	contents.add(Session);
	            contents.add(imageLabel);
	            contents.add(backG).setBounds(0,0,width,height);
	            repaint();
		    }
		});
		close.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	try {
					Runtime.getRuntime().exec("Taskkill /IM Robocopy.exe /F");
					System.exit(0);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    }
		});
		Session.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        if (fileChooser.getSelectedFile() != null)
	            {
		        	int l=0;
		        	if(fileChooser.getSelectedFile().getAbsolutePath().indexOf("ID") >= 0)
		        	{
		        		for(int q=fileChooser.getSelectedFile().getAbsolutePath().indexOf("ID");q<fileChooser.getSelectedFile().getAbsolutePath().length();q++)
		        		{
		        			if(fileChooser.getSelectedFile().getAbsolutePath().charAt(q) == '\\')
		        			{
		        				l=q-1; 
		        				break;
		        			}
		        			l=q;
		        		}
		        	}
		        if(fileChooser.getSelectedFile().getAbsolutePath().indexOf("ID") >= 0)
	        	{
		        	
	        	fileChooser.setSelectedFile(new File(fileChooser.getSelectedFile().getAbsolutePath().substring(0,l+1)));
	        	DirName = fileChooser.getSelectedFile();
	        	String response = "";
				try {
					response = HttpsClient.testIt("?SESSION="+URLEncoder.encode(DirName.getAbsolutePath().replace(SFTPDISKNAME+":\\", ""),"UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	if(response.length() > 0)
	        	{
		        		String[] resp = response.split("X{5}");
	    		    	p_title.setText(resp[0]);
	    		    	speaker.setText(resp[1]);
	        	}	
		        fileSelect.removeAll(fileSelect);
	        	contents.removeAll();
	        	contents.add(showPPT);
	        	contents.add(btnOpenDir);
	        	contents.add(Clear);
	        	contents.add(close);
	        	contents.add(Session);
	            contents.add(imageLabel);
	            String[] dirList = DirName.list(new FilenameFilter(){

					@Override
					public boolean accept(File dir, String filename) {
						return new File(dir.getAbsolutePath()+ "\\"+filename).isDirectory();
				}});
	            Arrays.sort(dirList,new WindowsExplorerStringComparator());
	        	JButton[] papers = new JButton[dirList.length];
	        	int Padding = 0;
	        	for(int n=0; n<dirList.length; n++)
	        	{
	        		String[] buttonText = dirList[n].split("-");
	        		papers[n] = new JButton(buttonText[buttonText.length-1]);
	        		papers[n].setActionCommand(dirList[n]);
	        		papers[n].setBounds(250+Padding,height - 100,(width-250)/dirList.length-10,75);
	        		papers[n].setFont(new Font("Arial", Font.PLAIN, 20));
	        		papers[n].setBackground(new Color(0,191,255));
	        		papers[n].setForeground(Color.WHITE);
	        		Padding+=(width-250)/dirList.length;
	        		papers[n].addActionListener(new ActionListener() {
	        		    public void actionPerformed(ActionEvent e) {
	        		    	fileSelect.removeAll(fileSelect);
	        		    	for(int n=0; n<dirList.length; n++)
	    		        	{
	        		    		if(papers[n].getActionCommand() == e.getActionCommand())
	        		    			{
	        		    				papers[n].setBackground(Color.ORANGE);
	        		    				papers[n].setForeground(Color.GRAY);
	        		    			}
	        		    		else
	        		    			{
	        		    				papers[n].setBackground(new Color(0,191,255));
	        		    				papers[n].setForeground(Color.WHITE);
	        		    			}
	    		        	}
	        		    	String response="";
							try {
								response = HttpsClient.testIt("?PATH="+URLEncoder.encode((DirName.getAbsolutePath()+"\\"+e.getActionCommand()).replace(SFTPDISKNAME+":\\", ""),"UTF-8"));
							} catch (UnsupportedEncodingException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							if(response.length() > 0)
	        		    	{
	        		    		String[] resp = response.split("X{5}");
		        		    	p_title.setText(resp[0]);
		        		    	speaker.setText(resp[1]);
	        		    	}
	        		    	File pres = new File(DirName.getAbsolutePath()+"\\"+e.getActionCommand());
	        		    	String[] preslist = pres.list(new FilenameFilter(){

								@Override
								public boolean accept(File dir, String name) {
									// TODO Auto-generated method stub
									for(int i=0; i<formats.size();i++)
									{
										if(name.indexOf(formats.get(i)) >=0)
										{
											dirSelect = dir.getAbsolutePath();
											fileSelect.add(name);
											return true;
										}
									}
									return false;
								}});
	        		    	if(preslist.length == 0)
	        		    		fileSelect.removeAll(fileSelect);
	        		    	repaint();
	        		    }
	        		});
	        		contents.add(papers[n]);
	        	}
	        	contents.add(p_title);
	        	contents.add(speaker);
	        	contents.add(backG).setBounds(0,0,width,height);
	            }
	        	else
	        	{
	        		JOptionPane.showMessageDialog(null, "Please, choose session!");
	        		btnOpenDir.doClick();
	        	}
	        	repaint();
	            }
		    }
		});
	}
	public static void main(String[] args)
    {
        // ����������� ����������� ���� JFileChooser
        UIManager.put(
                 "FileChooser.saveButtonText", "Save");
        UIManager.put(
                 "FileChooser.cancelButtonText", "Cancel");
        UIManager.put(
                 "FileChooser.fileNameLabelText", "File name");
        UIManager.put(
                 "FileChooser.filesOfTypeLabelText", "File types");
        UIManager.put(
                 "FileChooser.lookInLabelText", "Directory");
        UIManager.put(
                 "FileChooser.saveInLabelText", "Save at directory");
        UIManager.put(
                 "FileChooser.folderNameLabelText", "Directory path");
        UIManager.put("FileChooser.readOnly", Boolean.TRUE);
        try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        new ShowPPT();
    }
}

class MyTimerTask extends TimerTask {

	@Override
	public void run() {
		if(new File(ShowPPT.SFTPDISKNAME+":\\").exists())
		{
			ShowPPT.fileChooser.setCurrentDirectory(new File(ShowPPT.SFTPDISKNAME+":\\"));
			System.out.println("Disk found");
			this.cancel();
		}
		else
			System.out.println("Disk not found");
		
	}
}