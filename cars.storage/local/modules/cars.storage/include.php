<?php
	
	class TestData
	{
		
		public $_brands;
		public $_models;
		public $_comps;
		public $_options;
		public $_cars;
		
		public function __construct(array $brands, array $models, array $comps, array $options, array $cars)
		{
			
			$this->_brands = $brands; 
			$this->_models = $models; 
			$this->_comps = $comps; 
			$this->_options = $options; 
			$this->_cars = $cars; 
			
		}
		
		public function insert()
		{
			
			$brandsID = array();
			$modelsID = array();
			$compsID = array();
			$optionsID = array();
			$carsID = array();
			
			
			/* brands */
			if(count($this->_brands) > 0){
				

				foreach($this->_brands as $key=>$brand)
				{
					
					$result = \Cars\Storage\BrandsTable::add(array(
						'BRAND_NAME' => $brand
					));

					if ($result->isSuccess()){
						
						$brandsID[$key] = $result->getId();
						
					}
					
				}
				
			}
			
			/* models */
			if(count($this->_models) > 0){
				

				foreach($this->_models as $key=>$model)
				{
					
					$result = \Cars\Storage\ModelsTable::add(array(
						'MODEL_NAME' => $model["NAME"],
						'BRAND_ID' => $brandsID[$model["BRAND"]]
					));

					if ($result->isSuccess()){
						
						$modelsID[$key] = $result->getId();
						
					}
					
				}
				
			}
			
			/* options */
			if(count($this->_options) > 0){
				

				foreach($this->_options as $key=>$option)
				{
					
					$result = \Cars\Storage\OptionsTable::add(array(
						'OPTION_NAME' => $option
					));

					if ($result->isSuccess()){
						
						$optionsID[$key] = $result->getId();
						
					}
					
				}
				
			}
			
			/* comps */
			if(count($this->_comps) > 0){
				

				foreach($this->_comps as $key=>$comp)
				{
					
					$result = \Cars\Storage\CompsTable::add(array(
						'COMP_NAME' => $comp["NAME"],
						'MODEL_ID' => $modelsID[$comp["MODEL"]]
					));

					if ($result->isSuccess()){
						
						$compsID[$key] = $result->getId();
						
						foreach($comp["OPTIONS"] as $option)
						{
							
							$result2 = \Cars\Storage\Comps2OptionsTable::add(array(
								'COMP_ID' => $compsID[$key],
								'OPTION_ID' => $optionsID[$option]
							));
							
						}
						
					}
					
				}
				
			}
			
			
			/* auto */
			if(count($this->_cars) > 0){
				

				foreach($this->_cars as $key=>$car)
				{
					
					$result = \Cars\Storage\CarsTable::add(array(
						'PRICE' => $car["PRICE"],
						'YEAR' => $car["YEAR"],
						'COMP_ID' => $compsID[$car["COMP"]]
					));

					if ($result->isSuccess()){
						
						$carsID[$key] = $result->getId();
						
						foreach($car["OPTIONS"] as $option)
						{
							
							$result2 = \Cars\Storage\Cars2OptionsTable::add(array(
								'CAR_ID' => $carsID[$key],
								'OPTION_ID' => $optionsID[$option]
							));
							
						}
						
					}
					
				}
				
			}
			
		}
		
	}