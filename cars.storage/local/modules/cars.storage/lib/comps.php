<?php
namespace Cars\Storage;
 
use Bitrix\Main\Entity;
 
class CompsTable extends Entity\DataManager
{
   /**
    * Returns DB table name for entity.
    *
    * @return string
    */
   public static function getTableName()
   {
      return 'comps';
   }
 
   /**
    * Returns entity map definition.
    *
    * @return array
    */
   public static function getMap()
   {
	   
		return array(
			//ID
			new Entity\IntegerField('COMP_ID', array(
				'primary' => true,
				'autocomplete' => true
			)),
			//Название
			new Entity\StringField('COMP_NAME', array(
				'required' => true
			)),
			//Модель
			new Entity\IntegerField('MODEL_ID'),
			//Привязка к модели
			new Entity\ReferenceField(
                'MODEL',
                '\Cars\Storage\ModelsTable',
                array('=this.MODEL_ID' => 'ref.MODEL_ID')
            ),
			//Привязка к комплектации
			new Entity\ReferenceField(
                'OPTION',
                '\Cars\Storage\Comps2OptionsTable',
                array('=this.COMP_ID' => 'ref.COMP_ID')
            )
		);
     
   }
   
}