<?php
namespace Cars\Storage;
 
use Bitrix\Main\Entity;
 
class OptionsTable extends Entity\DataManager
{
   /**
    * Returns DB table name for entity.
    *
    * @return string
    */
   public static function getTableName()
   {
      return 'options';
   }
 
   /**
    * Returns entity map definition.
    *
    * @return array
    */
   public static function getMap()
   {
	   
		return array(
			//ID
			new Entity\IntegerField('OPTION_ID', array(
				'primary' => true,
				'autocomplete' => true
			)),
			//Название
			new Entity\StringField('OPTION_NAME', array(
				'required' => true
			)),
			//Привязка к комплектации
			new Entity\ReferenceField(
                'OPTION_COMPS',
                '\Cars\Storage\Comps2Options',
                array('=this.OPTION_ID' => 'ref.OPTION_ID')
            ),
			//Привязка к авто
			new Entity\ReferenceField(
                'OPTION_CARS',
                '\Cars\Storage\Cars2Options',
                array('=this.OPTION_ID' => 'ref.OPTION_ID')
            )
		);
     
   }
   
}