<?php
namespace Cars\Storage;
 
use Bitrix\Main\Entity;
 
class ModelsTable extends Entity\DataManager
{
   /**
    * Returns DB table name for entity.
    *
    * @return string
    */
   public static function getTableName()
   {
      return 'models';
   }
 
   /**
    * Returns entity map definition.
    *
    * @return array
    */
   public static function getMap()
   {
	   
		return array(
			//ID
			new Entity\IntegerField('MODEL_ID', array(
				'primary' => true,
				'autocomplete' => true
			)),
			//Название
			new Entity\StringField('MODEL_NAME', array(
				'required' => true
			)),
			//Брэнд
			new Entity\IntegerField('BRAND_ID'),
			//Привязка к брэнду
			new Entity\ReferenceField(
                'BRAND',
                '\Cars\Storage\BrandsTable',
                array('=this.BRAND_ID' => 'ref.BRAND_ID')
            )
		);
     
   }
   
}