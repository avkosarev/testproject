<?php
namespace Cars\Storage;
 
use Bitrix\Main\Entity;
 
class BrandsTable extends Entity\DataManager
{
   /**
    * Returns DB table name for entity.
    *
    * @return string
    */
   public static function getTableName()
   {
      return 'brands';
   }
 
   /**
    * Returns entity map definition.
    *
    * @return array
    */
   public static function getMap()
   {
	   
		return array(
			//ID
			new Entity\IntegerField('BRAND_ID', array(
				'primary' => true,
				'autocomplete' => true
			)),
			//Название
			new Entity\StringField('BRAND_NAME', array(
				'required' => true
			))
		);
     
   }
   
}