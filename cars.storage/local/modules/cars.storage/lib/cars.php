<?php
namespace Cars\Storage;
 
use Bitrix\Main\Entity;
 
class CarsTable extends Entity\DataManager
{
   /**
    * Returns DB table name for entity.
    *
    * @return string
    */
   public static function getTableName()
   {
      return 'cars';
   }
 
   /**
    * Returns entity map definition.
    *
    * @return array
    */
   public static function getMap()
   {
	   
		return array(
			//ID
			new Entity\IntegerField('CAR_ID', array(
				'primary' => true,
				'autocomplete' => true
			)),
			//Цена
			new Entity\IntegerField('PRICE'),
			//Год
			new Entity\IntegerField('YEAR'),
			//Комплектация
			new Entity\IntegerField('COMP_ID'),
			//Привязка к комплектации
			new Entity\ReferenceField(
                'COMP',
                '\Cars\Storage\CompsTable',
                array('=this.COMP_ID' => 'ref.COMP_ID')
            ),
			//Привязка к опции
			new Entity\ReferenceField(
                'CAR',
                '\Cars\Storage\Cars2OptionsTable',
                array('=this.CAR_ID' => 'ref.CAR_ID')
            )
		);
     
   }
   
}