<?php
namespace Cars\Storage;
 
use Bitrix\Main\Entity;
 
class Comps2OptionsTable extends Entity\DataManager
{
   /**
    * Returns DB table name for entity.
    *
    * @return string
    */
   public static function getTableName()
   {
      return 'comps2options';
   }
 
   /**
    * Returns entity map definition.
    *
    * @return array
    */
   public static function getMap()
   {
	   
		return array(

            new Entity\IntegerField('COMP_ID', array(
				'primary' => true
			)),
            new Entity\ReferenceField(
                'COMP',
                '\Cars\Storage\CompsTable',
                array('=this.COMP_ID' => 'ref.COMP_ID')
            ),
            new Entity\IntegerField('OPTION_ID', array(
				'primary' => true
			)),
            new Entity\ReferenceField(
                'OPTION',
                 '\Cars\Storage\OptionsTable',
                array('=this.OPTION_ID' => 'ref.OPTION_ID')
            )
        );
     
   }
   
}