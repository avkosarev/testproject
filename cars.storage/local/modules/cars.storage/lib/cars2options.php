<?php
namespace Cars\Storage;
 
use Bitrix\Main\Entity;
 
class Cars2OptionsTable extends Entity\DataManager
{
   /**
    * Returns DB table name for entity.
    *
    * @return string
    */
   public static function getTableName()
   {
      return 'cars2options';
   }
 
   /**
    * Returns entity map definition.
    *
    * @return array
    */
   public static function getMap()
   {
	   
		return array(

            new Entity\IntegerField('CAR_ID', array(
				'primary' => true
			)),
            new Entity\ReferenceField(
                'CAR',
                '\Cars\Storage\CarsTable',
                array('=this.CAR_ID' => 'ref.CAR_ID')
            ),
            new Entity\IntegerField('OPTION_ID', array(
				'primary' => true
			)),
            new Entity\ReferenceField(
                'OPTION',
                 '\Cars\Storage\OptionsTable',
                array('=this.OPTION_ID' => 'ref.OPTION_ID')
            )
        );
     
   }
   
}