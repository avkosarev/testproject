<?php
$MESS["CARS_STORAGE_MODULE_NAME"] = "Модуль Хранение автомобилей";
$MESS["CARS_STORAGE_MODULE_DESCRIPTION"] = "Модуль Хранение автомобилей";
$MESS["CARS_STORAGE_PARTNER_NAME"] = "Косарев Антон";
$MESS["CARS_STORAGE_PARTNER_URI"] = "localhost";

$MESS["CARS_STORAGE_DENIED"] = "Доступ закрыт";
$MESS["CARS_STORAGE_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["CARS_STORAGE_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["CARS_STORAGE_FULL"] = "Полный доступ";

$MESS["CARS_STORAGE_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["CARS_STORAGE_UNINSTALL_TITLE"] = "Удалить таблицы?";
$MESS["CARS_STORAGE_INSTALL_TITLE"] = "Установка модуля";