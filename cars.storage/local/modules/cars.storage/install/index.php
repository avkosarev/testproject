<?php 

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;
use \Bitrix\Main\ModuleManager;
use \Bitrix\Main\Request;
use \Bitrix\Main\Context;

Loc::loadMessages(__FILE__); 

Class cars_storage extends CModule
{
 
    function __construct()
    {
		
		$arModuleVersion = array();
		include(__DIR__."/version.php");
		
		$this->MODULE_ID = "cars.storage";
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("CARS_STORAGE_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("CARS_STORAGE_MODULE_DESCRIPTION");
		
		$this->PARTNER_NAME = Loc::getMessage("CARS_STORAGE_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("CARS_STORAGE_PARTNER_URI");
		
    }
 
    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot=false)
    {
        if($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(),'',dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    function InstallDB()
    {
		
        Loader::includeModule($this->MODULE_ID);

        if(!Application::getConnection(\Cars\Storage\BrandsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Cars\Storage\BrandsTable')->getDBTableName()
            )
        )
        {
            Base::getInstance('\Cars\Storage\BrandsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Cars\Storage\ModelsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Cars\Storage\ModelsTable')->getDBTableName()
            )
        )
        {
            Base::getInstance('\Cars\Storage\ModelsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Cars\Storage\CompsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Cars\Storage\CompsTable')->getDBTableName()
            )
        )
        {
            Base::getInstance('\Cars\Storage\CompsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Cars\Storage\CarsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Cars\Storage\CarsTable')->getDBTableName()
            )
        )
        {
            Base::getInstance('\Cars\Storage\CarsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Cars\Storage\OptionsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Cars\Storage\OptionsTable')->getDBTableName()
            )
        )
        {
            Base::getInstance('\Cars\Storage\OptionsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Cars\Storage\Cars2OptionsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Cars\Storage\Cars2OptionsTable')->getDBTableName()
            )
        )
        {
            Base::getInstance('\Cars\Storage\Cars2OptionsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Cars\Storage\Comps2OptionsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Cars\Storage\Comps2OptionsTable')->getDBTableName()
            )
        )
        {
            Base::getInstance('\Cars\Storage\Comps2OptionsTable')->createDbTable();
        }

    }

    function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        Application::getConnection(\Cars\Storage\BrandsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Cars\Storage\BrandsTable')->getDBTableName());
			
		Application::getConnection(\Cars\Storage\ModelsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Cars\Storage\ModelsTable')->getDBTableName());	
			
		Application::getConnection(\Cars\Storage\CompsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Cars\Storage\CompsTable')->getDBTableName());
					
		Application::getConnection(\Cars\Storage\CarsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Cars\Storage\CarsTable')->getDBTableName());	
			
		Application::getConnection(\Cars\Storage\OptionsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Cars\Storage\OptionsTable')->getDBTableName());

		Application::getConnection(\Cars\Storage\Cars2OptionsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Cars\Storage\Cars2OptionsTable')->getDBTableName());	

		Application::getConnection(\Cars\Storage\Comps2OptionsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Cars\Storage\Comps2OptionsTable')->getDBTableName());		

        Option::delete($this->MODULE_ID);
    }

	function DoInstall()
	{
		global $APPLICATION;
        if($this->isVersionD7())
        {
			
			$request = Context::getCurrent()->getRequest();
		
			if( $request->isPost() && $request->getPost("SUBMIT_STEP_INSTALL") == "Y" ){
				
				
				\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
				
				
				if( $request->getPost("DELETE") == "Y" ){
					
					$this->UnInstallDB();
					
				}
			
				$this->InstallDB();
				$this->InstallFiles();
				
				
				/* TEST DATA */
				$brands = array(
		
					0 => "Brand1",
					1 => "Brand2"
				
				);
				
				$models = array(
				
					0 => array( "NAME" => "Model1", "BRAND" => 0 ),
					1 => array( "NAME" => "Model2", "BRAND" => 1 ),
					2 => array( "NAME" => "Model3", "BRAND" => 0 ),
					3 => array( "NAME" => "Model4", "BRAND" => 1 ),
					4 => array( "NAME" => "Model5", "BRAND" => 0 ),
					5 => array( "NAME" => "Model6", "BRAND" => 1 ),
				
				);
				
				$options = array(
				
					0 => "Option1",
					1 => "Option2",
					2 => "Option3",
					3 => "Option4",
					4 => "Option5"
					
				);
				
				$comps = array(
				
					0 => array( "NAME" => "Comp1", "OPTIONS" => array(0,4), "MODEL" => 0 ),
					1 => array( "NAME" => "Comp2", "OPTIONS" => array(1,3), "MODEL" => 1 ),
					2 => array( "NAME" => "Comp3", "OPTIONS" => array(2,0), "MODEL" => 2 ),
					3 => array( "NAME" => "Comp4", "OPTIONS" => array(2,3), "MODEL" => 3 ),
					4 => array( "NAME" => "Comp5", "OPTIONS" => array(1,4), "MODEL" => 4 ),
					5 => array( "NAME" => "Comp6", "OPTIONS" => array(2,4), "MODEL" => 5 ),
					
				);
				
				$auto = array(
				
					0 => array( "PRICE" => 15000, "YEAR" => 2012,  "OPTIONS" => array( 0,1 ), "COMP" => 0),
					1 => array( "PRICE" => 23052, "YEAR" => 2004,  "OPTIONS" => array( 1,2 ), "COMP" => 1),
					2 => array( "PRICE" => 31563, "YEAR" => 1995,  "OPTIONS" => array( 2,3 ), "COMP" => 2),
					3 => array( "PRICE" => 76523, "YEAR" => 1993,  "OPTIONS" => array( 3,4 ), "COMP" => 3),
					4 => array( "PRICE" => 89653, "YEAR" => 1994,  "OPTIONS" => array( 4,0 ), "COMP" => 4),
					5 => array( "PRICE" => 12456, "YEAR" => 1998,  "OPTIONS" => array( 0,1 ), "COMP" => 5),
					6 => array( "PRICE" => 46721, "YEAR" => 1999,  "OPTIONS" => array( 1,2 ), "COMP" => 0),
					7 => array( "PRICE" => 32151, "YEAR" => 1991,  "OPTIONS" => array( 2,3 ), "COMP" => 1),
					8 => array( "PRICE" => 56783, "YEAR" => 2001,  "OPTIONS" => array( 3,4 ), "COMP" => 2),
					9 => array( "PRICE" => 12457, "YEAR" => 1995,  "OPTIONS" => array( 4,0 ), "COMP" => 3),
					10 => array( "PRICE" => 23146, "YEAR" => 2008,  "OPTIONS" => array( 0,1 ), "COMP" => 4),
					11 => array( "PRICE" => 53263, "YEAR" => 1975,  "OPTIONS" => array( 1,2 ), "COMP" => 5),
					12 => array( "PRICE" => 21454, "YEAR" => 1980,  "OPTIONS" => array( 2,3 ), "COMP" => 0)
				
				);
				

				
				$test = new TestData($brands,$models,$comps,$options,$auto);
				
				$test->insert();
				/* TEST DATA */
				
			}
			else{
				
				$APPLICATION->IncludeAdminFile(Loc::getMessage("CARS_STORAGE_INSTALL_TITLE"), $this->GetPath()."/install/step_install.php");
				
			}
			

        }
        else
        {
            $APPLICATION->ThrowException(Loc::getMessage("CARS_STORAGE_INSTALL_ERROR_VERSION"));
        }
	}

	function DoUninstall()
	{
		global $APPLICATION;
		
		$request = Context::getCurrent()->getRequest();
		
		if( $request->isPost() && $request->getPost("SUBMIT_STEP_UNINSTALL") ){
				

			if( $request->getPost("SUBMIT_STEP_UNINSTALL") == "Y" )	
				$this->UnInstallDB();
				
			\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
			
		}
		else{
			
			$APPLICATION->IncludeAdminFile(Loc::getMessage("CARS_STORAGE_UNINSTALL_TITLE"), $this->GetPath()."/install/step_delete.php");
			
		}
		

	}
	
	function InstallFiles($arParams = array())
	{
        $path=$this->GetPath()."/install/components";

        if(\Bitrix\Main\IO\Directory::isDirectoryExists($path))
            CopyDirFiles($path, $_SERVER["DOCUMENT_ROOT"]."/local/components", true, true);
        else
            throw new \Bitrix\Main\IO\InvalidPathException($path);
		
		$path=$this->GetPath()."/install/api";

        if(\Bitrix\Main\IO\Directory::isDirectoryExists($path))
            CopyDirFiles($path, $_SERVER["DOCUMENT_ROOT"]."/api", true, true);
        else
            throw new \Bitrix\Main\IO\InvalidPathException($path);
		
		CUrlRewriter::Add( array(
			"CONDITION" => "#^/api/#",
			"RULE" => "",
			"ID" => "avkosarev:api.cars",
			"PATH" => "/api/index.php",
		));

        return true;
	}

    function GetModuleRightList()
    {
        return array(
            "reference_id" => array("D","K","S","W"),
            "reference" => array(
                "[D] ".Loc::getMessage("CARS_STORAGE_DENIED"),
                "[K] ".Loc::getMessage("CARS_STORAGE_READ_COMPONENT"),
                "[S] ".Loc::getMessage("CARS_STORAGE_WRITE_SETTINGS"),
                "[W] ".Loc::getMessage("CARS_STORAGE_FULL"))
        );
    }
}
