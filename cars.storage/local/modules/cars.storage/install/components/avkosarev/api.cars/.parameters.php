<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//Список полей для сортировки
$arSorts = array("ASC"=>GetMessage("SORT_ASC"), "DESC"=>GetMessage("SORT_DESC"));
$arSortFields = array(
		 
		"PRICE"=>GetMessage("SORT_PRICE"),
		"YEAR"=>GetMessage("SORT_YEAR"),
);
 
$arComponentParameters = array(
 
	"PARAMETERS" => array(
	
		"SORT_BY1" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("SORT_BY1"),
			"TYPE" => "LIST",
			"VALUES" => $arSortFields
		),
		"SORT_ORDER1" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("SORT_ORDER1"),
			"TYPE" => "LIST",
			"DEFAULT" => "DESC",
			"VALUES" => $arSorts
		),
		"VARIABLE_ALIASES" => array(
			 "TABLE" => array(
				"NAME" => GetMessage("VAR_TABLE"),
			 ),
			 "CAR_ID" => array(
				"NAME" => GetMessage("VAR_CAR"),
			 )
		),
		"SEF_MODE" => array(
		
			"list" => array(
				"NAME" => GetMessage("SEF_MODE_TABLE"),
				"DEFAULT" => "#TABLE#",
				"VARIABLES" => array()
			),
			"detail" => array(
				"NAME" => GetMessage("SEF_MODE_DETAIL"),
				"DEFAULT" => "cars/#CAR_ID#",
				"VARIABLES" => array()
			),
			
		 )
	)
	
);
