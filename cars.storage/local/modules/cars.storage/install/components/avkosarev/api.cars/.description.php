<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 

$arComponentDescription = array(
	"NAME" => GetMessage("CARS_STORAGE_NAME"),
	"DESCRIPTION" => GetMessage("CARS_STORAGE_DESCRIPTION"),
	"PATH" => array(
		"ID" => "avkosarev_components",
		"CHILD" => array(
			"ID" => "cars",
			"NAME" => GetMessage("CARS_STORAGE_COMPLEX_COMPONENT")
		)
	),
);
?>