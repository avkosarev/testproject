<?
$MESS["CARS_STORAGE_TABLE"] = "Выбор сущности";
$MESS["SORT_ASC"] = "По возрастанию";
$MESS["SORT_DESC"] = "По убыванию";
$MESS["SORT_PRICE"] = "По цене";
$MESS["SORT_YEAR"] = "По году выпуска";
$MESS["SORT_BY1"] = "Поле сортировки";
$MESS["SORT_ORDER1"] = "Направление сортировки";
$MESS["CARS_COUNT"] = "Количество автомобилей на странице";
$MESS["VAR_TABLE"] = "Переменная таблицы";
$MESS["VAR_CAR"] = "Переменная автомобиля";
$MESS["SEF_MODE_TABLE"] = "ЧПУ для таблицы";
$MESS["SEF_MODE_DETAIL"] = "ЧПУ для авто";
?>