<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use \Bitrix\Main\Loader;
use \Bitrix\Main\Request;
use \Bitrix\Main\Context;

Loader::includeModule("cars.storage");

$request = Context::getCurrent()->getRequest();

if( $request->getRequestMethod() !== "GET" ){
	
	http_response_code(404);
	exit;
	
}

$Data = array();

$obData = \Cars\Storage\CarsTable::getList( array(

	"filter" => array(
	
		"CAR_ID" => $arParams["CAR_ID"]
	
	),
	"select" => array(
	
		"CAR_ID",
		"PRICE",
		"YEAR",
		"OPTIONS" => "CAR.OPTION.OPTION_NAME",
		"COMPLECTATION" => "COMP.COMP_NAME",
		"MODEL" => "COMP.MODEL.MODEL_NAME",
		"BRAND" => "COMP.MODEL.BRAND.BRAND_NAME",
		"OPTIONS FROM COMP" => "COMP.OPTION.OPTION.OPTION_NAME"
		
	)

));

while($arData = $obData->fetch())
{
	
	$Data = $arData;
	
	$Options["OPTIONS FROM COMP"][] = $arData["OPTIONS FROM COMP"];
	
	$Options["OPTIONS"][] = $arData["OPTIONS"];
	
}

$Data["OPTIONS"] = array_unique($Options["OPTIONS"]);
$Data["OPTIONS FROM COMP"] = array_unique($Options["OPTIONS FROM COMP"]);

$arResult = $Data;


$this->includeComponentTemplate();