<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();header("HTTP/1.0 404 Not Found");
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddHeadString("<script type='text/javascript' src='" . $this->GetFolder() . "/js/jquery.js'></script>");
$APPLICATION->AddHeadString("<script type='text/javascript' src='" . $this->GetFolder() . "/js/bootstrap.min.js'></script>");
$APPLICATION->AddHeadString("<link rel='stylesheet' type='text/css' href='" . $this->GetFolder() . "/css/bootstrap.min.css' />");


?>

<div class="container">

	<div class="alert alert-info">
	
			<b><?=GetMessage("ITEMS_SEARCH");?>:<b/> <br>
			
			<?foreach($arResult["ITEMS_SEARCH"] as $Key=>$Value):?>
			
				<?=$Key;?>: <?=$Value;?> <br>
			
			<?endforeach;?>	
				
	
	</div>
			

<?foreach( $arResult["ITEMS"] as $arItem ){
	
	?>
		<div class="alert alert-success">
		
				<?foreach($arItem as $Key=>$Value):?>
					
					<?if( strpos($Key,"_NAME") !== false ):?>
						
						<a href="<?=$Value;?>/">
							<b><?=$Key;?>:</b>&nbsp;<?=$Value;?>
						</a>
						
					<?else:?>
					
						<p>
							<b><?=$Key;?>:</b>&nbsp;<?=$Value;?>
						</p>
					
					<?endif;?>

					
				<?endforeach;?>
				
				
				<?if( $arResult["DETAIL_URL"] ):?>

					<a href="<?=str_replace("#CAR_ID#",$arItem["CAR_ID"],$arResult["DETAIL_URL"]);?>">
						DETAIL_URL
					</a>
				
				<?endif;?>	
		
		</div>
			
	<?
}?>

	<?if( $arResult["BACK_URL"] ):?>

		<a class="btn btn-primary" href="<?=$arResult["BACK_URL"];?>">
			BACK_URL
		</a>
				
	<?endif;?>

</div>
