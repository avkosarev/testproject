<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddHeadString("<script type='text/javascript' src='" . $this->GetFolder() . "/js/jquery.js'></script>");
$APPLICATION->AddHeadString("<script type='text/javascript' src='" . $this->GetFolder() . "/js/bootstrap.min.js'></script>");
$APPLICATION->AddHeadString("<link rel='stylesheet' type='text/css' href='" . $this->GetFolder() . "/css/bootstrap.min.css' />");


?>

<div class="container">

		<div class="alert alert-success">
		
				<?foreach($arResult as $Key=>$Value):?>
					
					<p>
						<?if(is_array($Value))
							echo $Key . " : " . implode(", ", $Value);
						else
							echo $Key . " : " . $Value;?>
					</p>	
					
				<?endforeach;?>
		
		</div>

</div>
