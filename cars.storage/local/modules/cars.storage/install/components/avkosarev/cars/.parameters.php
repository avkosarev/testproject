<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


//Список полей для сортировки
$arSorts = array("ASC"=>GetMessage("SORT_ASC"), "DESC"=>GetMessage("SORT_DESC"));
$arSortFields = array(
		 
		"PRICE"=>GetMessage("SORT_PRICE"),
		"YEAR"=>GetMessage("SORT_YEAR"),
);
 
$arComponentParameters = array(
 
	"PARAMETERS" => array(
	
		"CARS_COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CARS_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => "20",
		),
		"SORT_BY1" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("SORT_BY1"),
			"TYPE" => "LIST",
			"VALUES" => $arSortFields
		),
		"SORT_ORDER1" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("SORT_ORDER1"),
			"TYPE" => "LIST",
			"DEFAULT" => "DESC",
			"VALUES" => $arSorts
		),
		"VARIABLE_ALIASES" => array(
			 "CAR_ID" => array(
				"NAME" => GetMessage("VAR_CAR"),
			 ),
			 "BRAND" => array(
				"NAME" => GetMessage("VAR_BRAND"),
			 ),
			 "MODEL" => array(
				"NAME" => GetMessage("VAR_MODEL"),
			 ),
			 "COMP" => array(
				"NAME" => GetMessage("VAR_COMP"),
			 )
		),
		"SEF_MODE" => array(
		
			"brands" => array(
				"NAME" => GetMessage("SEF_MODE_BRANDS"),
				"DEFAULT" => "index.php",
				"VARIABLES" => array()
			),
			"models" => array(
				"NAME" => GetMessage("SEF_MODE_MODELS"),
				"DEFAULT" => "#BRAND#/",
				"VARIABLES" => array()
			),
			"comps" => array(
				"NAME" => GetMessage("SEF_MODE_COMPS"),
				"DEFAULT" => "#BRAND#/#MODEL#/#COMP#/",
				"VARIABLES" => array()
			),
			"cars" => array(
				"NAME" => GetMessage("SEF_MODE_CARS"),
				"DEFAULT" => "#BRAND#/#MODEL#/",
				"VARIABLES" => array()
			),
			"detail" => array(
				"NAME" => GetMessage("SEF_MODE_CAR_DETAIL"),
				"DEFAULT" => "detail/#CAR_ID#",
				"VARIABLES" => array()
			),
			
		 )
	)
	
);
