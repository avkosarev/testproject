<?
$MESS["CARS_STORAGE_TABLE"] = "Выбор сущности";
$MESS["SORT_ASC"] = "По возрастанию";
$MESS["SORT_DESC"] = "По убыванию";
$MESS["SORT_PRICE"] = "По цене";
$MESS["SORT_YEAR"] = "По году выпуска";
$MESS["SORT_BY1"] = "Поле сортировки";
$MESS["SORT_ORDER1"] = "Направление сортировки";
$MESS["CARS_COUNT"] = "Количество автомобилей на странице";
$MESS["VAR_CAR"] = "Переменная автомобиля";
$MESS["VAR_BRAND"] = "Переменная брэнда";
$MESS["VAR_MODEL"] = "Переменная модели";
$MESS["VAR_COMP"] = "Переменная комплектации";
$MESS["SEF_MODE_BRANDS"] = "ЧПУ для брэндов";
$MESS["SEF_MODE_MODELS"] = "ЧПУ для моделей";
$MESS["SEF_MODE_COMPS"] = "ЧПУ для комплектаций";
$MESS["SEF_MODE_CARS"] = "ЧПУ для автомобилей";
$MESS["SEF_MODE_CAR_DETAIL"] = "ЧПУ для деталльного просмотра автомобиля";
?>