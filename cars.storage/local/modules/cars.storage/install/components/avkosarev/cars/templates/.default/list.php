<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?$APPLICATION->IncludeComponent(
	"avkosarev:cars.list",
	".default",
	Array(
		"TABLE" => $arResult["TABLE"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $arParams["SORT_BY1"],
		"SORT_ORDER1" => $arParams["SORT_ORDER1"],
		"CARS_COUNT" => $arParams["CARS_COUNT"],
		"VARIABLES" => $arResult["VARIABLES"],
		"URL_TEMPLATES" => $arResult["URL_TEMPLATES"],
		"FOLDER" => $arResult["FOLDER"],
		"SEF_MODE" => "Y"
	),
	$component
);?>
 