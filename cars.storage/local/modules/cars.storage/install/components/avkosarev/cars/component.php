<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Loader;

Loader::includeModule("iblock");



$arDefaultUrlTemplates404 = array();

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array();


$SEF_FOLDER = "";
$arUrlTemplates = array();

if ($arParams["SEF_MODE"] == "Y")
{
   $arVariables = array();

   $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
   $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

   $componentPage = CComponentEngine::ParseComponentPath(
      $arParams["SEF_FOLDER"],
      $arUrlTemplates,
      $arVariables
   );

   if (StrLen($componentPage) <= 0)
      Bitrix\Iblock\Component\Tools::process404(null, true, true, true);

   CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

   $SEF_FOLDER = $arParams["SEF_FOLDER"];
   
   if($componentPage != "detail")
      $componentPage = "list";
}
else
{
   $arVariables = array();

   $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
   CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

   $componentPage = "";
   if (IntVal($arVariables["CAR_ID"]) > 0)
      $componentPage = "detail";
   else
      $componentPage = "list";

}

$arResult = array(
   "FOLDER" => $SEF_FOLDER,
   "URL_TEMPLATES" => $arUrlTemplates,
   "VARIABLES" => $arVariables,
   "ALIASES" => $arVariableAliases
);

$this->IncludeComponentTemplate($componentPage);
?>