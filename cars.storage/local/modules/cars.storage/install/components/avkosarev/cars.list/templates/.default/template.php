<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();header("HTTP/1.0 404 Not Found");
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddHeadString("<script type='text/javascript' src='" . $this->GetFolder() . "/js/jquery.js'></script>");
$APPLICATION->AddHeadString("<script type='text/javascript' src='" . $this->GetFolder() . "/js/bootstrap.min.js'></script>");
$APPLICATION->AddHeadString("<link rel='stylesheet' type='text/css' href='" . $this->GetFolder() . "/css/bootstrap.min.css' />");


?>

<div class="container">
			

<?foreach( $arResult["ITEMS"] as $arItem ){
	
	?>
		<div class="alert alert-success">
		
				<?foreach($arItem as $Key=>$Value):?>
				
					<p>
						<b><?=$Key;?>:</b>&nbsp;<?=$Value;?>
					</p>

					
				<?endforeach;?>
				
				
				<?if( $arResult["DETAIL_URL"] ):?>

					<a href="<?=str_replace("#CAR_ID#",$arItem["CAR_ID"],$arResult["DETAIL_URL"]);?>">
						DETAIL_URL
					</a>
				
				<?endif;?>	
		
		</div>
			
	<?
}?>

</div>
