<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//Список сущностей
$arTablesList = array(

	"brands" => "[brands] Брэнды",
	"models" => "[models] Модели",
	"comps" => "[comps] Комплектации",
	"cars" => "[cars] Автомобили",
	"options" => "[options] Опции"
	
);

//Список полей для сортировки
$arSorts = array("ASC"=>GetMessage("SORT_ASC"), "DESC"=>GetMessage("SORT_DESC"));
$arSortFields = array(
		 
		"PRICE"=>GetMessage("SORT_PRICE"),
		"YEAR"=>GetMessage("SORT_YEAR"),
);
 
$arComponentParameters = array(
 
	"PARAMETERS" => array(
	
		"TABLE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CARS_STORAGE_TABLE"),
			"TYPE" => "LIST",
			"MULTIPLE" => "N",
			"VALUES" => $arTablesList
			
		),
		"CARS_COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CARS_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"SORT_BY1" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("SORT_BY1"),
			"TYPE" => "LIST",
			"VALUES" => $arSortFields
		),
		"SORT_ORDER1" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("SORT_ORDER1"),
			"TYPE" => "LIST",
			"DEFAULT" => "DESC",
			"VALUES" => $arSorts
		),
		"DETAIL_PAGE_URL" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DETAIL_PAGE_URL"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		)
	)
	
);
