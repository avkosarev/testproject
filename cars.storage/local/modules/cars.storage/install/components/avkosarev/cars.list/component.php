<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader;

Loader::includeModule("cars.storage");
Loader::includeModule("iblock");

$Data = array();

$nav = new \Bitrix\Main\UI\PageNavigation("nav-more-news");
		$nav->allowAllRecords(true)
		   ->setPageSize($arParams["CARS_COUNT"])
		   ->initFromUri();

if($arParams["SEF_MODE"] == "Y"){
	
	
	if( array_key_exists( "COMP", $arParams["VARIABLES"] ) ){
	
		$filter = array(
				
				"COMP.COMP_NAME" => $arParams["VARIABLES"]["COMP"] 
				
		);
		
		$obData = \Cars\Storage\CarsTable::getList( array(
		
			"filter" => $filter,
			"order" => array(
			
				$arParams["SORT_BY1"] => $arParams["SORT_ORDER1"]
			
			),
			'offset' => $nav->getOffset(),
			'limit' => $nav->getLimit(),
			"count_total" => true
			
		));
		
		$arResult["DETAIL_URL"] = $arParams["FOLDER"] . $arParams["URL_TEMPLATES"]["detail"];

		$arResult["BACK_URL"] = str_replace($arParams["VARIABLES"]["COMP"] . "/", "", $APPLICATION->GetCurUri());	

		
	}elseif( array_key_exists( "MODEL", $arParams["VARIABLES"] ) ){
		
		
		$filter = array(
				
				"MODEL.MODEL_NAME" => $arParams["VARIABLES"]["MODEL"] 
				
		);
		
		$obData = \Cars\Storage\CompsTable::getList( array(
		
			"filter" => $filter,
			'offset' => $nav->getOffset(),
			'limit' => $nav->getLimit(),
			"count_total" => true
			
		));
		
		$arResult["BACK_URL"] = str_replace($arParams["VARIABLES"]["MODEL"] . "/", "", $APPLICATION->GetCurUri());

		
	}elseif( array_key_exists( "BRAND", $arParams["VARIABLES"] ) ){
		
		
		$filter = array(
				
				"BRAND.BRAND_NAME" => $arParams["VARIABLES"]["BRAND"]
				
		);
		
		$obData = \Cars\Storage\ModelsTable::getList( array(
		
			"filter" => $filter,
			'offset' => $nav->getOffset(),
			'limit' => $nav->getLimit(),
			"count_total" => true
			
		));
		
		$arResult["BACK_URL"] = str_replace($arParams["VARIABLES"]["BRAND"] . "/", "", $APPLICATION->GetCurUri());
		
	}else{
		

		$obData = \Cars\Storage\BrandsTable::getList( array(
		
			'offset' => $nav->getOffset(),
			'limit' => $nav->getLimit(),
			"count_total" => true
			
		));
		
		$arResult["BACK_URL"] = "";
		
	}

		
}else{
	
	switch( $arParams["TABLE"] ){
		
		case "brands":
		
			$obData = \Cars\Storage\BrandsTable::getList( array(
			
				'offset' => $nav->getOffset(),
				'limit' => $nav->getLimit(),
				"count_total" => true
								
			));
		
		break;
		
		case "models":
		
			$filter = array(
				
				"BRAND.BRAND_NAME" => $arParams["VARIABLES"]["BRAND"]
				
			);

			$obData = \Cars\Storage\ModelsTable::getList( array(
			
				'offset' => $nav->getOffset(),
				'limit' => $nav->getLimit(),
				"count_total" => true
				
			));
			
		break;
		
		case "comps":
		
			$obData = \Cars\Storage\CompsTable::getList( array(
			
				'offset' => $nav->getOffset(),
				'limit' => $nav->getLimit(),
				"count_total" => true
				
			));
		
		break;
		
		case "cars":

			$obData = \Cars\Storage\CarsTable::getList( array(
			
				"order" => array(
				
					$arParams["SORT_BY1"] => $arParams["SORT_ORDER1"]
				
				),
				'offset' => $nav->getOffset(),
				'limit' => $nav->getLimit(),
				"count_total" => true
				
			));

			
			$arResult["DETAIL_URL"] = $arParams["DETAIL_PAGE_URL"];
		
		break;
		
		case "options":
		
			$obData = \Cars\Storage\OptionsTable::getList( array(
			
				'offset' => $nav->getOffset(),
				'limit' => $nav->getLimit(),
				"count_total" => true
				
			));
		
		break;
		
		default:
			
			Bitrix\Iblock\Component\Tools::process404(null, true, true, true);
			
		break;
		
	}
	
}

$nav->setRecordCount($obData->getCount());

$Data = $obData->fetchAll();

$APPLICATION->IncludeComponent(
   "bitrix:main.pagenavigation",
   "",
   array(
	  "NAV_OBJECT" => $nav,
	  "SEF_MODE" => "N",
   ),
   false
);

$arResult["ITEMS"] = $Data;
$arResult["ITEMS_SEARCH"] = $arParams["VARIABLES"];

$this->includeComponentTemplate();