<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader;
use \Bitrix\Main\Request;
use \Bitrix\Main\Context;

Loader::includeModule("cars.storage");
Loader::includeModule("iblock");

$Data = array();

$request = Context::getCurrent()->getRequest();

if( $request->getRequestMethod() !== "GET" ){
	
	http_response_code(404);
	exit;
	
}

switch( $arParams["VARIABLES"]["TABLE"] ){
	
	case "brands":

		$Data = \Cars\Storage\BrandsTable::getList()->fetchAll();
	
	break;
	
	case "models":
	
		$filter = array();
		
		if( $request->get("brand") )
			$filter["BRAND.BRAND_ID"] = $request->get("brand");
		
		$Data = \Cars\Storage\ModelsTable::getList( array(
		
			"filter" => $filter
			
		))->fetchAll();	
		
	break;
	
	case "comps":	
		
		$filter = array();
		
		if( $request->get("model") )
			$filter["MODEL.MODEL_ID"] = $request->get("model");
		
		$Data = \Cars\Storage\CompsTable::getList( array(
		
			"filter" => $filter
			
		))->fetchAll();
	
	break;
	
	case "cars":
	
		$filter = array();
		
		if( $request->get("model") )
			$filter["COMP.MODEL.MODEL_ID"] = $request->get("model");
		
		if( $request->get("comp") )
			$filter["COMP.COMP_ID"] = $request->get("comp");
		
		if( $request->get("brand") )
			$filter["COMP.MODEL.BRAND.BRAND_ID"] = $request->get("brand");
		
		if( $request->get("year") )
			$filter["YEAR"] = $request->get("year");
		
		if( $request->get("price") )
			$filter["PRICE"] = $request->get("price");
		
		$Data = \Cars\Storage\CarsTable::getList( array(
		
			"order" => array(
			
				$arParams["SORT_BY1"] => $arParams["SORT_ORDER1"]
			
			),
			"filter" => $filter
			
		))->fetchAll();
	
	break;
	
	
	default:
		
		http_response_code(404);
		exit;
		
	break;
	
}

$arResult["ITEMS"] = $Data;

$this->includeComponentTemplate();