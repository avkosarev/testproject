<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
 
$arComponentParameters = array(
 
	"PARAMETERS" => array(
	
		"CAR_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CAR_ID"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => '={$_REQUEST["CAR_ID"]}'
			
		)
	)
	
);
