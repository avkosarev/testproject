<?php 

	use \Bitrix\Main\Localization\Loc; 
	
	if (!check_bitrix_sessid())
		return;

?>

<form action="" method="POST">
	<button type="submit" value="Y" name="SUBMIT_STEP_UNINSTALL"><?=Loc::getMessage("DELETE_YES"); ?></button>
	<button type="submit" value="N" name="SUBMIT_STEP_UNINSTALL"><?=Loc::getMessage("DELETE_NO"); ?></button>
<form>