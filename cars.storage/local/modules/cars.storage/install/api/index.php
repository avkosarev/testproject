<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->IncludeComponent(
	"avkosarev:api.cars", 
	".default", 
	array(
		"CARS_COUNT" => "1",
		"SORT_BY1" => "PRICE",
		"SORT_ORDER1" => "DESC",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/api/",
		"COMPONENT_TEMPLATE" => ".default",
		"SEF_URL_TEMPLATES" => array(
			"list" => "#TABLE#",
			"detail" => "cars/#CAR_ID#",
		)
	),
	false
);
?>