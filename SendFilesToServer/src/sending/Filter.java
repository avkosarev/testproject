package sending;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Filter extends JFrame{
	private static final long serialVersionUID = 1L;
	MyDrawPanel select = new MyDrawPanel();
	JButton OK = new JButton("Search");
	JButton myPapers = new JButton("My Reports");
	JButton CLOSE = new JButton("Close");
    String[] itemsDays;
    String[] itemsHalls;
    String[] itemsHallsID;
    String[] itemsSessions;
    String[] itemsSessionsID;
    
    public static JComboBox comboBoxDays; 
    public static JComboBox comboBoxHalls;
    public static JComboBox comboBoxSessions;
    
	public Filter()
	{
		super("Filter");
    	String[] GetInfo = (HttpsClient.testIt("?GET_SECTIONS=Y&COMP_ID="+SendFileToServer.CompID)).split("Y{3}");
    	
    	itemsSessions = GetInfo[3].split("X{3}");
    	itemsSessionsID = GetInfo[4].split("X{3}");
    	itemsHalls = GetInfo[0].split("X{3}");
    	itemsHallsID = GetInfo[1].split("X{3}");
    	itemsDays = GetInfo[2].split("X{3}");

        comboBoxDays = new JComboBox(itemsDays); 
        comboBoxHalls = new JComboBox(itemsHalls);
        comboBoxSessions = new JComboBox(itemsSessions);
        
		SendFileToServer.Session = itemsSessions[comboBoxSessions.getSelectedIndex()];
		SendFileToServer.Hall = itemsHalls[comboBoxHalls.getSelectedIndex()];
		SendFileToServer.SessionID = itemsSessionsID[comboBoxSessions.getSelectedIndex()];
		SendFileToServer.HallID = itemsHallsID[comboBoxHalls.getSelectedIndex()];
		SendFileToServer.Day = itemsDays[comboBoxDays.getSelectedIndex()];
		
		OK.setPreferredSize(new Dimension(SendFileToServer.width/4,100));
		OK.setFont(new Font("Arial", Font.PLAIN, 40));
		CLOSE.setPreferredSize(new Dimension(SendFileToServer.width/4,100));
		CLOSE.setFont(new Font("Arial", Font.PLAIN, 40));
		myPapers.setPreferredSize(new Dimension(SendFileToServer.width/4,100));
		myPapers.setFont(new Font("Arial", Font.PLAIN, 40));
    	comboBoxDays.setFont(new Font("Arial", Font.PLAIN, 20));
    	comboBoxHalls.setFont(new Font("Arial", Font.PLAIN, 20));
    	comboBoxSessions.setFont(new Font("Arial", Font.PLAIN, 20));
    	comboBoxDays.setPreferredSize(new Dimension(SendFileToServer.width-20,100));
    	comboBoxHalls.setPreferredSize(new Dimension(SendFileToServer.width-20,100));
    	comboBoxSessions.setPreferredSize(new Dimension(SendFileToServer.width-20,100));
        setContentPane(select);
        setUndecorated(true);
        select.setBackground(new Color(214,225,247));
        select.setBorder(new EmptyBorder(SendFileToServer.height/2 - 200, 20,20, 20));      
        select.add(comboBoxDays,BorderLayout.CENTER);
        select.add(comboBoxHalls,BorderLayout.CENTER);
        select.add(comboBoxSessions,BorderLayout.CENTER);
        select.add(OK,BorderLayout.CENTER);
        select.add(myPapers,BorderLayout.CENTER);
        select.add(CLOSE,BorderLayout.CENTER);
        setSize(SendFileToServer.width,SendFileToServer.height);
        setVisible(false);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    	
        OK.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	
		    	SendFileToServer.Session = itemsSessions[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
		    	SendFileToServer.SessionID = itemsSessionsID[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
				SendFileToServer.Hall = itemsHalls[comboBoxHalls.getSelectedIndex()];
				SendFileToServer.HallID = itemsHallsID[comboBoxHalls.getSelectedIndex()];
				SendFileToServer.Day = itemsDays[comboBoxDays.getSelectedIndex()];
		    	String Split = "";
				try {
					Split = HttpsClient.testIt("?COMP_ID="+SendFileToServer.CompID+"&SESSION="+URLEncoder.encode(SendFileToServer.SessionID,"UTF-8")+"&DAY="+URLEncoder.encode(SendFileToServer.Day,"UTF-8") + "&HALL="+URLEncoder.encode(SendFileToServer.HallID,"UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    	if(!Split.equals("N"))
	        	{
		    	String[] arSplit = Split.split("Y{3}");
	        	String[] arNames = arSplit[0].split("X{3}");
	        	SendFileToServer.paperNames.removeAll(SendFileToServer.paperNames);
	        	for(int i=0; i< arNames.length;i++)
	        	{
	        		SendFileToServer.paperNames.add(new MyBean(arNames[i]));
	        	}
	        	SendFileToServer.arHalls = arSplit[1].split("X{3}");
	        	SendFileToServer.arHallsID = arSplit[2].split("X{3}");
	        	SendFileToServer.arDates = arSplit[3].split("X{3}");
	        	SendFileToServer.arSort = arSplit[4].split("X{3}");
	        	SendFileToServer.arSections = arSplit[5].split("X{3}");
	        	SendFileToServer.arSectionsID = arSplit[6].split("X{3}");
	        	}
		    	else SendFileToServer.paperNames.removeAll(SendFileToServer.paperNames);
		    	SendFileToServer.paperScrollPane.updateUI();
		    	setVisible(false);
		    }
		    });
        myPapers.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	SendFileToServer.paperNames.removeAll(SendFileToServer.paperNames);
	        	String Split = HttpsClient.testIt("?COMP_ID="+SendFileToServer.CompID+"&ID="+SendFileToServer.LOGIN.getText());
	        	if(!Split.equals("N"))
	        	{
	        	String[] arSplit = Split.split("Y{3}");
	        	String[] arNames = arSplit[0].split("X{3}");
	        	for(int i=0; i< arNames.length;i++)
	        	{
	        		SendFileToServer.paperNames.add(new MyBean(arNames[i]));
	        	}
	        	SendFileToServer.arHalls = arSplit[1].split("X{3}");
	        	SendFileToServer.arHallsID = arSplit[2].split("X{3}");
	        	SendFileToServer.arDates = arSplit[3].split("X{3}");
	        	SendFileToServer.arSort = arSplit[4].split("X{3}");
	        	SendFileToServer.arSections = arSplit[5].split("X{3}");
	        	SendFileToServer.arSectionsID = arSplit[6].split("X{3}");
	        	}
	        	else
	        		SendFileToServer.paperNames.removeAll(SendFileToServer.paperNames);
	        	SendFileToServer.paperScrollPane.updateUI();
	        	setVisible(false);
		    }
		    });
        CLOSE.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	SendFileToServer.paperScrollPane.updateUI();
		    	setVisible(false);
		    }
		    });
        comboBoxSessions.addItemListener(new ItemListener() {
		    public void itemStateChanged(ItemEvent  e) {
		    	if(comboBoxSessions.getSelectedIndex() >=0)
		    	{
		    		if(comboBoxSessions.getItemCount() > 0)
			    	{
		    			SendFileToServer.Session = itemsSessions[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
				    	SendFileToServer.SessionID = itemsSessionsID[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
				    	OK.setEnabled(true);
			    	}
		    		else
		    			OK.setEnabled(false);
		    		
			    	SendFileToServer.Hall = itemsHalls[comboBoxHalls.getSelectedIndex()];
			    	SendFileToServer.HallID = itemsHallsID[comboBoxHalls.getSelectedIndex()];
					SendFileToServer.Day = itemsDays[comboBoxDays.getSelectedIndex()];
		    	}
		    }
		});
        comboBoxDays.addItemListener(new ItemListener() {
		    public void itemStateChanged(ItemEvent  e) {
		    	comboBoxSessions.removeAllItems();
		    	for(int i=0; i<itemsSessions.length;i++)
		    	{
		    		if( itemsSessions[i].indexOf((String) comboBoxDays.getSelectedItem()) >=0 && itemsSessions[i].indexOf((String) comboBoxHalls.getSelectedItem()) >=0 )
		    			comboBoxSessions.addItem(itemsSessions[i]);
		    	}
		    	if(comboBoxSessions.getItemCount() > 0)
		    	{
		    		SendFileToServer.Session = itemsSessions[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
			    	SendFileToServer.SessionID = itemsSessionsID[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
			    	OK.setEnabled(true);
		    	}
		    	else
	    			OK.setEnabled(false);
		    	
		    	SendFileToServer.Hall = itemsHalls[comboBoxHalls.getSelectedIndex()];
		    	SendFileToServer.HallID = itemsHallsID[comboBoxHalls.getSelectedIndex()];
				SendFileToServer.Day = itemsDays[comboBoxDays.getSelectedIndex()];
		    }
		});
		comboBoxHalls.addItemListener(new ItemListener() {
		    public void itemStateChanged(ItemEvent  e) {
		    	comboBoxSessions.removeAllItems();
		    	for(int i=0; i<itemsSessions.length;i++)
		    	{
		    		if( itemsSessions[i].indexOf((String) comboBoxDays.getSelectedItem()) >=0 && itemsSessions[i].indexOf((String) comboBoxHalls.getSelectedItem()) >=0 )
		    			comboBoxSessions.addItem(itemsSessions[i]);
		    	}
		    	if(comboBoxSessions.getItemCount() > 0)
		    	{
		    		SendFileToServer.Session = itemsSessions[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
			    	SendFileToServer.SessionID = itemsSessionsID[java.util.Arrays.asList(itemsSessions).indexOf((String) comboBoxSessions.getSelectedItem())];
			    	OK.setEnabled(true);
		    	}
		    	else
	    			OK.setEnabled(false);
		    	
		    	SendFileToServer.Hall = itemsHalls[comboBoxHalls.getSelectedIndex()];
		    	SendFileToServer.HallID = itemsHallsID[comboBoxHalls.getSelectedIndex()];
				SendFileToServer.Day = itemsDays[comboBoxDays.getSelectedIndex()];
				
		    }
		});
        
		ItemEvent e = null;
		comboBoxDays.getItemListeners()[0].itemStateChanged(e);
		comboBoxHalls.getItemListeners()[0].itemStateChanged(e);
		comboBoxSessions.getItemListeners()[0].itemStateChanged(e);
	}
}
class MyDrawPanel extends JPanel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paintComponent(Graphics g){ //графический метод.
    
	BufferedImage img = null;
    try {
        img = ImageIO.read(new File("back.jpg"));
    } catch (IOException e) {
    }
    g.drawImage(img, 0, 0, SendFileToServer.width, SendFileToServer.height, null);

} 
}

