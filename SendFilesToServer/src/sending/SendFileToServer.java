package sending;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.TrayIcon.MessageType;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;

public class SendFileToServer extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static String SFTPHOST;
    static int SFTPPORT;
    static String SFTPLOGIN;
    static String SFTPPASSWORD;
    static String SFTPWORKINGDIR;
    static String SFTPDISKNAME;
    static String SFTPPPTPATH;
    static String CompID;
    static String RequestPath;
    static String[] Parameters = new String[9];
	public  static JButton  btnLoadFile   = null;
	public  static JButton  btnOpenDir    = null;
	public  static JButton  btnPreview    = null;
	public  static JButton  btnClose    = null;
	public  static JButton  btnSearch    = null;
	public static JButton Next = null;
	public static JButton Back= null; 
	public static JButton Delete= null; 
    public static File FileName = null;
    public static  JFileChooser fileChooser = null;
    private JLabel imageLabel = new JLabel(new ImageIcon("logo.png"));
    private JLabel pathLabel = new JLabel("");
    private JLabel Title = new JLabel("<html><p style='font-weight:bold; text-align:center;'>Preview & Upload<br> Your Presentation Materials</p></html>");
    private JLabel Info = new JLabel("<html>"
    		+ "If your presentation file containes video clip(s), audio file(s) and/or internet link(s), please inform our staff before you complete the uploading.<br><br>"
    		+ "Once upload your file, it will be on the network server and will be accessible from computer in each session room."
    		+ "</html>");
    static JLabel addFile = new JLabel();
    private JLabel backG = new JLabel(new ImageIcon("back.jpg"));
    private JLabel listFiles = new JLabel();
     static JTextField LOGIN = new JTextField();
    private boolean Nexts[] = new boolean[5];
    public static int pageNumber = 0;
    static String Day ="";
    static String Order = "";
    static String Hall ="";
    static String Session ="";
    static String SessionID ="";
    static String HallID ="";
    static Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    static int width = (int)dim.getWidth();
    static int height = (int)dim.getHeight();
 // ���������� ������ � ����������
    public static JPanel contents = new JPanel();
    static ArrayList<MyBean> beans = new ArrayList<MyBean>();
    static ArrayList<MyBean> paperNames = new ArrayList<MyBean>();
    int SelectedRow = -1;
    static String[] arHalls;
    static String[] arHallsID;
    static String[] arDates;
    static String[] arSort;
    static String[] arSections;
    static String[] arSectionsID;
    TableModel Papers;
    JTable PapersList;
    static JScrollPane paperScrollPane;
    public static TableModel model;
    static JTable table;
    static JScrollPane scrollPane;
    ListSelectionModel table_selectionModel;
    ListSelectionModel PapersList_selectionModel;
    public static JOptionPane pane;
    public static JDialog dialog;
    public static ArrayList<String> formats = new ArrayList<String>();
    public static Filter filter;
    
    public SendFileToServer() {        
        super("File Uploading");
        
        BufferedReader format = null;
        try {
        	format = new BufferedReader(
                        new InputStreamReader(
                            new FileInputStream("formats.txt"), Charset.forName("UTF-8")));
            String line;
            while ((line = format.readLine()) != null) {
            	formats.add(line);
            }
        } catch (IOException e) {
            // log error
        } finally {
            if (format != null) {
                try {
                	format.close();
                } catch (IOException e) {
                    // log warning
                }
            }
        }
        
    	BufferedReader reader = null;
        int FileRow = 0;
        try {
            reader = new BufferedReader(
                        new InputStreamReader(
                            new FileInputStream("Settings.txt"), Charset.forName("UTF-8")));
            String line;
            while ((line = reader.readLine()) != null) {

           	 String[] Row = line.split("=");
                Parameters[FileRow++] = Row[1];
            }
        } catch (IOException e) {
            // log error
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // log warning
                }
            }
        }
        SFTPHOST=Parameters[0];
        SFTPPORT=Integer.parseInt(Parameters[1]);
        SFTPLOGIN=Parameters[2];
        SFTPPASSWORD=Parameters[3];
        SFTPWORKINGDIR=Parameters[4];
        SFTPDISKNAME=Parameters[5];
        SFTPPPTPATH=Parameters[6];
        CompID=Parameters[7];
        RequestPath=Parameters[8];
        
        filter = new Filter();
        
        for(int i=0; i<Nexts.length;i++)
        {
        	if(i==0)
        	Nexts[i]=true;
        	else
        		Nexts[i]=false;
        }
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        setUndecorated(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        model = new MyTableModel(beans);
        table = new JTable(model);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table_selectionModel = table.getSelectionModel();
        scrollPane = new JScrollPane(table);
        table.setBounds(20,300,width-40,height/3);
        table.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 30));
        table.setFont(new Font("Arial", Font.PLAIN, 30));
        table.getColumnModel().getColumn(0).setWidth(width-20);
        table.setRowHeight(40);
        table.setFillsViewportHeight(true);
        scrollPane.setBounds(20,250,width-40,height/3);
        Papers = new MyTableModel(paperNames);
        PapersList = new JTable(Papers);
        PapersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        PapersList_selectionModel = PapersList.getSelectionModel();
        paperScrollPane = new JScrollPane(PapersList);
        PapersList.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 25));
        PapersList.setFont(new Font("Arial", Font.PLAIN, 20));
        PapersList.getColumnModel().getColumn(0).setWidth(width-20);
        PapersList.setRowHeight(25);
        PapersList.setFillsViewportHeight(true);
        paperScrollPane.setBounds(20,250,width-40,height-200-250);
        Next = new JButton("Next");
        Back = new JButton("Back");
        Delete = new JButton("Delete");
        contents.setLayout(null);
        // ������ ������ ��������
        btnSearch = new JButton("Search");
        btnSearch.setBounds(width/2-200, height-170, 400, 50);
        btnSearch.setFont(new Font("Arial", Font.PLAIN, 25));
        //
        btnClose = new JButton("Close");
        btnClose.setBounds(width/2+10, height-100, 200, 70);
        btnClose.setFont(new Font("Arial", Font.PLAIN, 30));
        // ������ �������� ����������� ���� ��� ������ ����������
        btnOpenDir = new JButton("Select File");
        btnOpenDir.setBounds(10,270+height/3,width/3-20,75);
        btnOpenDir.setFont(new Font("Arial", Font.PLAIN, 30));
        // ������ �������������
        btnPreview = new JButton("Preview");
        btnPreview.setBounds(width/3+width/3+10,270+height/3,width/3-20,75);
        btnPreview.setFont(new Font("Arial", Font.PLAIN, 30));
        // ������ �������� ����������� ���� ��� ���������� �����
        btnLoadFile = new JButton("Send File");
        btnLoadFile.setBounds(10+width/3,270+height/3,width/3-20,75);
        btnLoadFile.setFont(new Font("Arial", Font.PLAIN, 30));
        // �������� ���������� JFileChooser 
        fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        /////
        pane = new JOptionPane("Message", JOptionPane.INFORMATION_MESSAGE);
        dialog = pane.createDialog(null, "Uploading");
        dialog.setModalityType(ModalityType.MODELESS);
        dialog.setSize(new Dimension(800,0));
        ////////////
        imageLabel.setHorizontalAlignment(SwingConstants.LEFT);
        imageLabel.setBounds(10, 10, width, 150);
        ////////
        // ����������� ���������� � �������
        addFileChooserListeners();
        addFile.setBounds(20,270+height/3+20,width-40,200);
        addFile.setFont(new Font("Arial", Font.PLAIN, 35));
        addFile.setForeground(Color.GRAY);
        ///////
        pathLabel.setBounds(10,height-20,width,20);
        pathLabel.setFont(new Font("Arial", Font.PLAIN, 10));
        ///////
        listFiles.setBounds(10,300,width-20,300);
        listFiles.setFont(new Font("Arial", Font.PLAIN, 30));
        listFiles.setBackground(Color.WHITE);
        listFiles.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        ////////
        Title.setHorizontalAlignment(SwingConstants.CENTER);
        Title.setFont(new Font("Arial", Font.PLAIN, 50));
        Title.setBounds(0,10,width,100); 
        ////////
        Info.setHorizontalAlignment(SwingConstants.CENTER);
        Info.setBounds(10,10,width-10-10,height-10-10);
        Info.setFont(new Font("Arial", Font.PLAIN, 40));
        ////////
        Next.setBounds(width/2-100, height-100, 200, 70);
        Next.setFont(new Font("Arial", Font.PLAIN, 30));
        ////////
        Back.setBounds(width/2-210, height-100, 200, 70);
        Back.setFont(new Font("Arial", Font.PLAIN, 30));
        //////
        LOGIN.setBounds(520,height-100-200,width-100-520,100);
    	LOGIN.setFont(new Font("Arial", Font.PLAIN, 40));
    	///////
        contents.add(imageLabel);
        contents.add(Title);
        contents.add(Info);
        contents.add(Next);
        ////////////////////////////////
        contents.add(backG).setBounds(0,0,width,height);
        setContentPane(contents);
        contents.setFocusable(true);
        // ����� ���� �� �����
        setSize(width,height );
        setVisible(true);
    }
    static void GetFiles()
    {
    	beans.removeAll(beans);
        Session     session     = null;
        Channel     channel     = null;
        ChannelSftp channelSftp = null;         
        try{
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPLOGIN,SFTPHOST,SFTPPORT);
            session.setPassword(SFTPPASSWORD);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;
            SftpATTRS attrs;
            try {
            	attrs = channelSftp.stat(SFTPWORKINGDIR+Day);
            }catch (Exception e) {
            	channelSftp.mkdir(SFTPWORKINGDIR+Day);
            }
            try {
            	attrs = channelSftp.stat(SFTPWORKINGDIR+Day+"/"+HallID);
            }catch (Exception e) {
            	channelSftp.mkdir(SFTPWORKINGDIR+Day+"/"+HallID);
            }
            try {
            	attrs = channelSftp.stat(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID);
            }catch (Exception e) {
            	channelSftp.mkdir(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID);
            }
            try {
            	attrs = channelSftp.stat(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order);
            }catch (Exception e) {
            	channelSftp.mkdir(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order);
            }
            channelSftp.cd(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/");
            Vector<?> filelist = channelSftp.ls(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/");
            for(int i=0; i<filelist.size();i++){
                LsEntry entry = (LsEntry) filelist.get(i);
                if(entry.getFilename().equals(".") || entry.getFilename().equals(".."))
                	continue;
                beans.add(new MyBean(entry.getFilename()));
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if (channel != null)
            {
                channel.disconnect();
            }
            if (session != null)
            {
                session.disconnect();
            }
        }
    }
    static String CheckFiles(String File)
    {
        Session     session     = null;
        Channel     channel     = null;
        ChannelSftp channelSftp = null;    
        try{
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPLOGIN,SFTPHOST,SFTPPORT);
            session.setPassword(SFTPPASSWORD);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;
            channelSftp.cd(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/");
            Vector<?> filelist = channelSftp.ls(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/");
            for(int i=0; i<filelist.size();i++){
                LsEntry entry = (LsEntry) filelist.get(i);
                if(entry.getFilename().equals(File))
                	return entry.getFilename();
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if (channel != null)
            {
                channel.disconnect();
            }
            if (session != null)
            {
                session.disconnect();
            }
        }
        return "";
    }
    private void DeleteFiles(String File)
    {
        Session     session     = null;
        Channel     channel     = null;
        ChannelSftp channelSftp = null;
        try{
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPLOGIN,SFTPHOST,SFTPPORT);
            session.setPassword(SFTPPASSWORD);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;
            channelSftp.rm(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/"+File);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if (channel != null)
            {
                channel.disconnect();
            }
            if (session != null)
            {
                session.disconnect();
            }
        }
    }
    public void SendFiles() {
    	dialog.setTitle("Please, wait. " + FileName.getName() + " is uploading...");
    	dialog.setVisible(true);
  		Set(false);
  		Delete.setEnabled(false);
        String SFTPDESTINATION = SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/"+FileName.getName();
        FileInputStream is = null;
        try {
			is = new FileInputStream(FileName.getPath());
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPLOGIN, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASSWORD);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;
            channelSftp.put(is, SFTPDESTINATION);       
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally{
            if (is != null) {
                try {
					is.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
            if (channel != null)
            {
                channel.disconnect();
            }
            if (session != null)
            {
                session.disconnect();
            }
        }
        GetFiles();
        scrollPane.updateUI();
        addFile.setText("");
        table.clearSelection();
        Set(true);
        contents.repaint();
        FileName = null;
        dialog.setVisible(false);
    }
    public static void Set(boolean flag)
    {
    	Back.setEnabled(flag);
    	btnClose.setEnabled(flag);
    	btnPreview.setEnabled(flag);
    	btnOpenDir.setEnabled(flag);
    	btnLoadFile.setEnabled(flag);
    	scrollPane.setEnabled(flag);
    }
	private void addFileChooserListeners() {
		
		contents.addKeyListener(new KeyAdapter() {
				public void keyReleased(KeyEvent e) {
					if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_P)
						System.exit(0);
					if(e.getKeyCode() == KeyEvent.VK_ENTER)
						Next.doClick();
					if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
						Back.doClick();
				}
        });
		PapersList_selectionModel.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e)
		    {
		    	SelectedRow = PapersList.getSelectedRow();
		    	if(SelectedRow >= 0)
		    	{
			    	Day = arDates[SelectedRow];
			    	Hall = arHalls[SelectedRow];
			    	HallID = arHallsID[SelectedRow];
			    	Session = arSections[SelectedRow];
			    	SessionID = arSectionsID[SelectedRow];
			    	Order = arSort[SelectedRow];
			    	Next.setEnabled(true);
		    	}
		    	else
		    		Next.setEnabled(false);
		    }
		});
		table_selectionModel.addListSelectionListener(new ListSelectionListener() {
		    public void valueChanged(ListSelectionEvent e)
		    {
		    	int rowIndex[] = table.getSelectedRows();
		    	if(rowIndex.length > 0)
		    	{
		    		Delete.setEnabled(true);
		    	}
		    	else
		    	{
		    		Delete.setEnabled(false);
		    	}
		    		
		    }
		});
		btnClose.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	pathLabel.setText("");
		    	pageNumber = 0;
		    	LOGIN.setText("");
		    	Day="";
		    	Hall="";
		    	HallID="";
		    	Session="";
		    	SessionID="";
		    	Order = "";
		    	contents.removeAll();
		    	Info.setBounds(10,10,width-10-10,height-10-10);
		    	Next.setBounds(width/2-100, height-100, 200, 70);
	        	Next.setEnabled(true);
	        	Title.setText("<html><p style='font-weight:bold; text-align:center;'>Preview & Upload<br> Your Presentation Materials</p></html>");
	        	Info.setText("<html>"
	            		+ "If your presentation file containes video clip(s), audio file(s) and/or internet link(s), please inform our staff before you complete the uploading.<br><br>"
	            		+ "Once upload your file, it will be on the network server and will be accessible from computer in each session room."
	            		+ "</html>");
	        	contents.add(imageLabel);
	            contents.add(Title);
	            contents.add(Info);
	            contents.add(Next);
	            contents.add(backG).setBounds(0,0,width,height);
	            repaint();
		    }
		    });
		btnSearch.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	Day="";
		    	Hall="";
		    	Session="";
		    	Order = "";
		    	PapersList.clearSelection();
		    	Next.setEnabled(false);
		    	filter.setVisible(true);
	            repaint();
		    }
		    });
		Next.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	pathLabel.setText(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/");
		    	Next.setFocusable(false);
		        contents.removeAll();
		        contents.add(pathLabel);
		        for(int i=0;i<Nexts.length;i++)
		        	Nexts[i] = false;
		        if(pageNumber == 4)
		        	pageNumber = 4;
		        else
		        pageNumber++;
		        Nexts[pageNumber] = true;
		        if(Nexts[0])
		        {
		        	Info.setBounds(10,10,width-10-10,height-10-10);
		        	Next.setBounds(width/2-100, height-100, 200, 70);
		        	Next.setEnabled(true);
		        	Title.setText("<html><p style='font-weight:bold; text-align:center;'>Preview & Upload<br> Your Presentation Materials</p></html>");
		        	Info.setText("<html>"
		            		+ "If your presentation file containes video clip(s), audio file(s) and/or internet link(s), please inform our staff before you complete the uploading.<br><br>"
		            		+ "Once upload your file, it will be on the network server and will be accessible from computer in each session room."
		            		+ "</html>");
		        	contents.add(imageLabel);
		            contents.add(Title);
		            contents.add(Info);
		            contents.add(Next);
		            contents.add(backG).setBounds(0,0,width,height);
		            contents.setFocusable(true);
		            contents.requestFocusInWindow();
		        }
		        if(Nexts[1])
		        {
		        	Title.setText("<html><p style='font-weight:bold;text-align:center'>Connection</p></html>");
		        	Info.setText("<html><p style='text-align:center'>Connect your USB flash drive and click the button below.</p></html>");
		        	Info.setBounds(10,10,width-10-10,height-10-10);
		        	Next.setBounds(width/2+10, height-100, 200, 70);
		        	Next.setEnabled(true);
		        	contents.add(Next);
		        	contents.add(Back);
		        	contents.add(Title);
		        	contents.add(Info);
		        	contents.add(imageLabel);
		        	contents.add(backG).setBounds(0,0,width,height);
		        	contents.setFocusable(true);
		        	contents.requestFocusInWindow();
		        	
		        } 
		        if(Nexts[2])
		        {
		        	JLabel RegNum = new JLabel("  Registration Number");
		        	RegNum.setFont(new Font("Arial", Font.PLAIN, 40));
		        	RegNum.setBounds(100,height-100-200,400,100);
		        	RegNum.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		        	Title.setText("<html><p style='font-weight:bold;text-align:center'>LOG-IN</p></html>");
		        	Info.setText("<html>"
		            		+ "Enter your registration number which is printed on your name tag.<br>"
		            		+ "If you have problems to log in, please inform our staff.<"
		            		+ "</html>");
		        	Info.setBounds(10,10,width-10-10,height-10-10);
		        	Next.setBounds(width/2+10, height-100, 200, 70);
		        	if(LOGIN.getText().length() == 0)
		        		Next.setEnabled(false);
		        	else
		        		Next.setEnabled(true);
		        	contents.add(Next);
		        	contents.add(Back);
		        	contents.add(Title);
		        	contents.add(Info);
		        	contents.add(imageLabel);
		        	contents.add(RegNum);
		        	contents.add(LOGIN);
		        	contents.add(backG).setBounds(0,0,width,height);
		        	contents.setFocusable(false);
		        	LOGIN.setFocusable(true);
		        }  
		        if(Nexts[3])
		        {
		        	SelectedRow = -1;
		        	Day="";
		        	Hall="";
		        	Session="";
		        	paperNames.removeAll(paperNames);
		        	String Split = HttpsClient.testIt("?COMP_ID="+CompID+"&ID="+LOGIN.getText());
		        	if(Split.length() == 0)
		        	{
		        		JOptionPane.showMessageDialog(null, "Please, check your Registration Number");
		        		Back.doClick();
		        	}
		        	else
		        	{
		        	if(!Split.equals("N"))
		        	{
		        	String[] arSplit = Split.split("Y{3}");
		        	String[] arNames = arSplit[0].split("X{3}");
		        	for(int i=0; i< arNames.length;i++)
		        	{
		        		paperNames.add(new MyBean(arNames[i]));
		        	}
		        	arHalls = arSplit[1].split("X{3}");
		        	arHallsID = arSplit[2].split("X{3}");
		        	arDates = arSplit[3].split("X{3}");
		        	arSort = arSplit[4].split("X{3}");
		        	arSections = arSplit[5].split("X{3}");
		        	arSectionsID = arSplit[6].split("X{3}");
		        	}
		        	else
		        		paperNames.removeAll(paperNames);
		            if(SelectedRow >=0)
		            	PapersList.changeSelection(SelectedRow, 0, false, false);
		            else
		            	PapersList.clearSelection();
		            
		        	contents.add(paperScrollPane);
		        	contents.add(btnSearch);
		        	Title.setText("<html><p style='font-weight:bold;text-align:center'>Find Your Report</p></html>");
		        	Info.setText("<html>"
		        	   		+ "If you didn't find your report, click 'Search'<br>"
		            		+ "Then click Next to upload your presentation material(s)"
		            		+ "</html>");
		        	Info.setBounds(10,10,width-10-10,300); 
		        	Next.setBounds(width/2+10, height-100, 200, 70);
		        	if(Day.length()==0 || Hall.length()==0 || Session.length()==0)
			        {
			        	Next.setEnabled(false);
			        }
			        else
			        	Next.setEnabled(true);
		        	contents.add(Next);
		        	contents.add(Back);
		        	contents.add(Title);
		        	contents.add(Info);
		        	contents.add(imageLabel);
		        	contents.add(backG).setBounds(0,0,width,height);
		        	contents.setFocusable(true);
		        	contents.requestFocusInWindow();
		        	}
		        }
		        if(Nexts[4])
		        {
		            Delete.setEnabled(false);
			        Delete.setBounds(width-300,190,200,50);
			        Delete.setFont(new Font("Arial", Font.PLAIN, 30));
		        	GetFiles();
		        	table.clearSelection();
		        	JLabel Choice = new JLabel(Session);
		        	Choice.setFont(new Font("Arial", Font.PLAIN, 25));
		        	Choice.setBounds(20,190,width-400,70);
		        	Title.setText("<html><p style='font-weight:bold;text-align:center'>Upload & Preview</p></html>");
		        	Next.setBounds(width/2+10, height-100, 200, 70);
		        	contents.add(btnClose);
		        	contents.add(Back);
		        	contents.add(Title);
		        	contents.add(imageLabel);
		        	contents.add(Choice);
		        	contents.add(Delete);
		            contents.add(addFile);
		            contents.add(btnOpenDir);
		            contents.add(btnPreview);
		            contents.add(btnLoadFile);
		            contents.add(scrollPane);
		            contents.add(backG).setBounds(0,0,width,height);
		            contents.setFocusable(true);
		            contents.requestFocusInWindow();
		        }
		        repaint();
		    }
		});
		Delete.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	if(table.getSelectedRows().length >= 0)
		    	{
		    		for(int i=0; i<table.getSelectedRows().length; i++)
		    		{
		    			DeleteFiles((String) model.getValueAt(table.getSelectedRows()[i], 0));
		    		}			    	
			    	GetFiles();
		            Delete.setEnabled(false);
		            table.clearSelection();
		    	}
		    	else
		    		JOptionPane.showMessageDialog(null, "Please, select file");
		    	scrollPane.updateUI();
		    	repaint();
		    }
		    });
		Back.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	pathLabel.setText(SFTPWORKINGDIR+Day+"/"+HallID+"/"+SessionID+"/"+Order+"/");
		    	Back.setFocusable(false);
		        contents.removeAll();
		        contents.add(pathLabel);
		        for(int i=0;i<Nexts.length;i++)
		        	Nexts[i] = false;
		        if(pageNumber == 0)
		        	pageNumber = 0;
		        else
		        pageNumber--;
		        Nexts[pageNumber] = true;
		        if(Nexts[0])
		        {
		        	Info.setBounds(10,10,width-10-10,height-10-10);
		        	Next.setBounds(width/2-100, height-100, 200, 70);
		        	Next.setEnabled(true);
		        	Title.setText("<html><p style='font-weight:bold; text-align:center;'>Preview & Upload<br> Your Presentation Materials</p></html>");
		        	Info.setText("<html>"
		            		+ "If your presentation file containes video clip(s), audio file(s) and/or internet link(s), please inform our staff before you complete the uploading.<br><br>"
		            		+ "Once upload your file, it will be on the network server and will be accessible from computer in each session room."
		            		+ "</html>");
		        	contents.add(imageLabel);
		            contents.add(Title);
		            contents.add(Info);
		            contents.add(Next);
		            contents.add(backG).setBounds(0,0,width,height);
		            contents.setFocusable(true);
		            contents.requestFocusInWindow();
		        }
		        if(Nexts[1])
		        {
		        	Title.setText("<html><p style='font-weight:bold;text-align:center'>Connection</p></html>");
		        	Info.setText("<html><p style='text-align:center'>Connect your USB flash drive and click the button below.</p></html>");
		        	Info.setBounds(10,10,width-10-10,height-10-10);
		        	Next.setBounds(width/2+10, height-100, 200, 70);
		        	Next.setEnabled(true);
		        	contents.add(Next);
		        	contents.add(Back);
		        	contents.add(Title);
		        	contents.add(Info);
		        	contents.add(imageLabel);
		        	contents.add(backG).setBounds(0,0,width,height);
		        	contents.setFocusable(true);
		        	contents.requestFocusInWindow();
		        } 
		        if(Nexts[2])
		        {
		        	JLabel RegNum = new JLabel("  Registration Number");
		        	RegNum.setFont(new Font("Arial", Font.PLAIN, 40));
		        	RegNum.setBounds(100,height-100-200,400,100);
		        	RegNum.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		        	Title.setText("<html><p style='font-weight:bold;text-align:center'>LOG-IN</p></html>");
		        	Info.setText("<html>"
		            		+ "Enter your registration number which is printed on your name tag.<br>"
		            		+ "If you have problems to log in, please inform our staff.<"
		            		+ "</html>");
		        	Info.setBounds(10,10,width-10-10,height-10-10);
		        	Next.setBounds(width/2+10, height-100, 200, 70);
		        	if(LOGIN.getText().length() == 0)
		        		Next.setEnabled(false);
		        	else
		        		Next.setEnabled(true);
		        	contents.add(Next);
		        	contents.add(Back);
		        	contents.add(Title);
		        	contents.add(Info);
		        	contents.add(imageLabel);
		        	contents.add(RegNum);
		        	contents.add(LOGIN);
		        	contents.add(backG).setBounds(0,0,width,height);
		        	contents.setFocusable(false);
		        	LOGIN.setFocusable(true);
		        }
		        if(Nexts[3])
		        {
		        	contents.add(paperScrollPane);
		        	contents.add(btnSearch);
		        	Title.setText("<html><p style='font-weight:bold;text-align:center'>Find Your Report</p></html>");
		        	Info.setText("<html>"
		        	   		+ "If you didn't find your report, click 'Search'<br>"
		            		+ "Then click Next to upload your presentation material(s)"
		            		+ "</html>");
		        	Info.setBounds(10,10,width-10-10,300);
		        	Next.setBounds(width/2+10, height-100, 200, 70);
		        	if(Day.length()==0 || Hall.length()==0 || Session.length()==0)
			        {
			        	Next.setEnabled(false);
			        }
			        else
			        	Next.setEnabled(true);
		        	contents.add(Next);
		        	contents.add(Back);
		        	contents.add(Title);
		        	contents.add(Info);
		        	contents.add(imageLabel);
		        	contents.add(backG).setBounds(0,0,width,height);
		        	contents.setFocusable(true);
		        	contents.requestFocusInWindow();
		        }
		        repaint();
		    }
		});
		// TODO Auto-generated method stub
		btnOpenDir.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        fileChooser.setDialogTitle("Choose File");
		        // ����������� ������ - ������ �������
		        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		        int result = fileChooser.showOpenDialog(SendFileToServer.this);
		        // ���� ���������� �������, ������� �� � ���������
		        if (result == JFileChooser.APPROVE_OPTION )
		        {
			        addFile.setText("You selected " + fileChooser.getSelectedFiles().length + " files for uploading");
		        }
		    }
		});
		btnLoadFile.addActionListener(new ActionListener()  {
		    public void actionPerformed(ActionEvent e)  {
		    	 if(fileChooser.getSelectedFiles().length == 0)
				    	JOptionPane.showMessageDialog(null, "Please, select file!");
		    	 else
		    	 {
				    for(int i=0; i<fileChooser.getSelectedFiles().length; i++)
				    {
				    	FileName = fileChooser.getSelectedFiles()[i];
				    	if(FileName!=null)	
					    {
							  	int res;
						    	if(CheckFiles(FileName.getName()).length() > 0)
						    	{
						    		res = JOptionPane.showConfirmDialog(null, "File " + CheckFiles(FileName.getName()) +" already exists. To rewrite it?");
						    	}
						    	else
						    	{
						    		res = 0;
						    	}
						    	if(res == 0)
						    	{
						            SendFiles();
						    	}		    	  
					    }
				    }
		    	 }
			    fileChooser.setSelectedFiles(null);
		    }
		});
		LOGIN.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_DELETE)
                	LOGIN.setText("");
                if(LOGIN.getText().length() == 0)
                {
                	Next.setEnabled(false);
                }
                else
                {
                	Next.setEnabled(true);
                }
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                	Next.doClick();
            }
        });
		btnPreview.addActionListener(new ActionListener() {
		      public void actionPerformed(ActionEvent e) {
		    	  if(table.getSelectedRows().length == 0)
		    		  JOptionPane.showMessageDialog(null, "Please, select file!");
		    	  else if(table.getSelectedRows().length > 1)
		    		  JOptionPane.showMessageDialog(null, "Please, select only one file!");
		    	  else if(table.getSelectedRows().length == 1)
		    		  	new PPTRead();
		        }
		      });
		
	}
	public static void main(String[] args)
    {
        // ����������� ����������� ���� JFileChooser
        UIManager.put(
                 "FileChooser.saveButtonText", "Save");
        UIManager.put(
                 "FileChooser.cancelButtonText", "Cancel");
        UIManager.put(
                 "FileChooser.fileNameLabelText", "File name");
        UIManager.put(
                 "FileChooser.filesOfTypeLabelText", "File types");
        UIManager.put(
                 "FileChooser.lookInLabelText", "Directory");
        UIManager.put(
                 "FileChooser.saveInLabelText", "Save at directory");
        UIManager.put(
                 "FileChooser.folderNameLabelText", "Directory path");
        UIManager.put("FileChooser.readOnly", Boolean.TRUE);
        try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        new SendFileToServer();
    }
}
