package sending;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class PPTRead extends JFrame {
	public PPTRead()
	{
			String fileSelect = ((String) SendFileToServer.model.getValueAt(SendFileToServer.table.getSelectedRow(), 0));
			boolean open = true;
			Runtime rt = Runtime.getRuntime();
	        try {
	        	
	        	for(int i=0; i<SendFileToServer.formats.size(); i++)
	        	{
	        		if(fileSelect.indexOf(SendFileToServer.formats.get(i)) >= 0)
	        		{
	        			open = false;
	        			rt.exec(new String[]{SendFileToServer.SFTPPPTPATH,"/s",SendFileToServer.SFTPDISKNAME+"\\"+SendFileToServer.Day+"\\"+SendFileToServer.HallID+"\\"+SendFileToServer.SessionID+"\\"+SendFileToServer.Order+"\\"+fileSelect});
	        			break;
	        		}
	        	}
	        	if(open)
	        	{
	        		Desktop.getDesktop().open(new File(SendFileToServer.SFTPDISKNAME+"\\"+SendFileToServer.Day+"\\"+SendFileToServer.HallID+"\\"+SendFileToServer.SessionID+"\\"+SendFileToServer.Order+"\\"+fileSelect));
	        	}

	        } catch (IllegalArgumentException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	            JOptionPane.showMessageDialog(null, "Please, check settings of file!","ERROR",JOptionPane.ERROR_MESSAGE);
	        } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		
	}

}