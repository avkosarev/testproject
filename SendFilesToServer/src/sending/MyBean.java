package sending;

public class MyBean {
	 
    private String name;

    public MyBean(String name) {
        this.setName(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

